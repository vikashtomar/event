(function (window, $) {
	$(document).on('click', '.club-install-theme', function(e) {
		e.preventDefault();
		var context = $(this);

		if ( context.hasClass('disabled') ) {
			return;
		}
		console.log('installing themee....');
		var download_url = context.data('url');
		var slug = context.data('slug');

		context.addClass('disabled');
		context.addClass('updating-message');
		context.text('Installing...');

		context.closest( '.theme' ).find( '.theme-install-error' ).remove();

		// Ajax to downlaod the theme
		$.ajax({
			url: pixcare.wpRest.base + 'rest_download_theme',
			method: 'POST',
			beforeSend: function(xhr){
				xhr.setRequestHeader('X-WP-Nonce', pixcare.wpRest.nonce);
			},
			data: {
				download_url: download_url,
				slug: slug,
				pixcare_nonce: pixcare.wpRest.pixcare_nonce
			},
			success: function(response) {
				if ( response && response.extracted === true ) {
					context.addClass('button-primary');
					context.removeClass('disabled');
					context.removeClass('club-install-theme');
					context.removeClass('updating-message');
					context.addClass('club-activate-theme');
					context.text('Activate');
				} else {// An error occurred
					if ( !_.isUndefined(response.errors) && !_.isUndefined(response.errors.errors) ) {
						context.removeClass('disabled');
						context.removeClass('updating-message');
						context.text('Install');

						var error_text = '';
						// Determine the message error
						switch(Object.keys(response.errors.errors)[0]) {
							case 'mkdir_failed_destination':
								error_text = 'The theme could not be installed. Please check if your current user has write permissions on the themes folder of your WordPress installation.'
								break;
							case 'no_package':
								error_text = response.errors.errors['no_package'];
								break;
							case 'download_failed':
								error_text = response.errors.errors['download_failed'];
								break;
							default:
								error_text = 'An unexpected error occured. Please contact us at help@pixelgrade.com';
								break;
						}

						// Add an error div
						context.closest( '.theme' ).find( '.theme-screenshot' ).prepend('<div class="error theme-install-error">' +  error_text + '</div>');
					}

				}
			}
		})
	});

	$(document).on('click', '.club-activate-theme', function(e) {
		e.preventDefault();

		var context = $(this);
		var slug = context.data('slug');

		// Ajax to download the theme
		$.ajax({
			url: pixcare.wpRest.base + 'rest_activate_theme',
			method: 'POST',
			beforeSend: function(xhr){
				xhr.setRequestHeader('X-WP-Nonce', pixcare.wpRest.nonce);
			},
			data: {
				slug: slug,
				pixcare_nonce: pixcare.wpRest.pixcare_nonce
			},
			success: function(response) {
				window.location.href = pixcare.themesUrl;
			}
		})
	});
})(window, jQuery);
