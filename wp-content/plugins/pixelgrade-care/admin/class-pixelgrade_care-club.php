<?php

class PixelgradeCare_Club {

	/**
	 * The main plugin object (the parent).
	 * @var     PixelgradeCare
	 * @access  public
	 * @since     1.3.0
	 */
	public $parent = null;

	/**
	 * The only instance.
	 * @var     PixelgradeCare_Club
	 * @access  protected
	 * @since   1.3.0
	 */
	protected static $_instance = null;

	private $is_club;
	private $license_status;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.3.0
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;

		$license_status = get_theme_mod( 'pixcare_license_status' );
		$license_type   = get_theme_mod( 'pixcare_license_type' );

		$this->is_club        = ! empty( $license_type ) && $license_type == 'shop_bundle' ? true : false;
		$this->license_status = ! empty( $license_status ) ? $license_status : false;
	}

	function club_enqueue_fe_scripts() {
		// If the license type is Pixelgrade club and license is expired enqueue the restriction notice scripts
		// We should only show this to logged in users that can actually do something about it
		if ( is_user_logged_in() && current_user_can( 'manage_options' ) && $this->is_club && ! in_array( $this->license_status, array(
				'valid',
				'active'
			) ) && ! PIXELGRADE_CARE__DEV_MODE ) {
			wp_enqueue_script( 'pixelgrade_care_club_fe', plugin_dir_url( $this->parent->file ) . 'admin/js/club/club-fe.js', array(), $this->parent->get_version(), true );
			wp_enqueue_style( 'pixelgrade_fonts', '//pxgcdn.com/fonts/galanogrotesquealt/stylesheet.css', array(), $this->parent->get_version(), 'all' );
			wp_enqueue_style( 'pixelgrade_care_club_fe_css', plugin_dir_url( $this->parent->file ) . 'admin/css/club/pixelgrade_care-club-fe.css', array(), $this->parent->get_version(), 'all' );

			$this->club_footer_content();

			$this->localize_club_js_data( 'pixelgrade_care_club_fe' );
		}
	}

	function club_enqueue_admin_scripts() {
		// We should only show this to logged in users that can actually do something about it
		if ( is_user_logged_in() && current_user_can( 'manage_options' ) && $this->is_club && ! in_array( $this->license_status, array(
				'valid',
				'active'
			) ) && ! PIXELGRADE_CARE__DEV_MODE ) {
			// If club license is expired - restrict the add new page view.

			if ( isset( $_GET['post_type'] ) && ! empty( $_GET['post_type'] ) && 'page' === $_GET['post_type'] || is_customize_preview() ) {
				wp_enqueue_style( 'pixelgrade_care_club_restrict', plugin_dir_url( $this->parent->file ) . 'admin/css/club/pixelgrade_care-club-restrict.css', array(), $this->parent->get_version(), 'all' );
			}
		}
	}

	private function localize_club_js_data( $handle = 'pixelgrade_care_club_fe' ) {
		$pixelgrade_club = array(
			'is_club'        => $this->is_club,
			'license_status' => $this->license_status
		);

		wp_localize_script( $handle, 'pixelgrade_club', $pixelgrade_club );
	}

	/**
	 * @param null $license_hash
	 *
	 * @return bool
	 * Helper function that helps enable the pixelgrade club themes
	 */
	static function enable_pixelgrade_club() {
		$license_hash = get_theme_mod( 'pixcare_license_hash' );

		// Get the activation user
		$current_user = PixelgradeCareAdmin::get_theme_activation_user();

		// Check if the user has any pixelgrade themes saved
		$user_themes = get_user_meta( $current_user->ID, 'pixelgrade_themes', 1 );
		// Try to get the themes available for this package
		if ( empty( $user_themes ) ) {
			$pixelgrade_user_id = get_user_meta( $current_user->ID, 'pixcare_user_ID', 1 );
			$club_themes        = self::get_user_themes_from_shop( $license_hash, $pixelgrade_user_id );
			$themes             = PixelgradeCareAdmin::filter_pixelgrade_themes( $club_themes );

			// update the user meta with the current themes they have access to
			update_user_meta( $current_user->ID, 'pixelgrade_themes', $themes );
		}
	}

	/**
	 * A hook to the update themes transient that updates the Pixelgrade themes this user has access to
	 */
	static function update_user_themes( $transient ) {
		// Nothing to do here if the checked transient entry is empty
		if ( empty( $transient->checked ) ) {
			return $transient;
		}
		$license_hash = get_theme_mod( 'pixcare_license_hash' );

		// Not connected - nothing to do.
		if ( ! $license_hash ) {
			return $transient;
		}
		// Fetch all the themes we have as part of the pixelgrade club
		$local_user_themes = get_option( 'pixelgrade_themes' );
		if ( ! $local_user_themes ) { // Not part of pixelgrade club - nothing to do here
			return $transient;
		}

		// Fetch the themes the user has access to
		$shop_user_themes = self::get_user_themes_from_shop();

		if ( empty( $shop_user_themes ) ) { // The shop did not give us the themes - return
			return $transient;
		}

		// Check if there have been any new additions or if any theme has been removed
		// Merge the two themes arrays
		$user_themes = self::merge_user_themes( $local_user_themes, $shop_user_themes );

		// Update the club themes option
		update_option( 'pixelgrade_themes', $user_themes );

		return $transient;
	}

	/**
	 * Output the content for the current step.
	 */
	public function club_footer_content() {
		?>
		<div id="pixelgrade_care_club_section"></div>
		<?php
	}

	/**
	 * @param null $license_hash
	 *
	 * @return array|bool|mixed|object
	 * Helper function that calls an endpoint and fetches the available thems for a specific user based on that user's license hash
	 */
	static function get_user_themes_from_shop( $license_hash = null, $user_id = null ) {
//		if ( ! $license_hash ) {
//			$license_hash = get_theme_mod( 'pixcare_license_hash' );
//		}

		$request = wp_remote_post( trailingslashit( PIXELGRADE_CARE__API_BASE ) . 'wp-json/wupl/v1/front/get_customer_products', array(
			'body'      => array(
//				'license_hash' => $license_hash,
				'user_id' => $user_id
			),
			'sslverify' => false,
		) );

		if ( is_wp_error( $request ) ) {
			return false;
		}

		$club_themes = json_decode( wp_remote_retrieve_body( $request ), true );

		return $club_themes;
	}

	/**
	 * @param $local_thems
	 * @param $shop_themes
	 *
	 * A helper function that updates the themes displayed as being available for use in the users Pixelgrade -> Themes section
	 */
	static function merge_user_themes( $local_themes, $shop_themes ) {
		// First remove the local themes that the user doesn't have access to anymore
		$updated_local_themes = $local_themes;
		foreach ( $local_themes as $local_theme ) {
			if ( ! in_array( $local_theme['slug'], $shop_themes ) ) {
				//remove the theme from our local theme
				unset( $updated_local_themes[ $local_theme['slug'] ] );
			}
		}

		// Second - if there are any new additions from the shop add them to the local themes array
		foreach ( $shop_themes as $key => $shop_theme ) {
			if ( ! in_array( $shop_theme['slug'], $updated_local_themes ) ) {
				$updated_local_themes[ $key ]['id']                   = isset( $shop_theme['slug'] ) ? $shop_theme['slug'] : null;
				$updated_local_themes[ $key ]['active']               = false;
				$updated_local_themes[ $key ]['name']                 = isset( $shop_theme['title'] ) ? $shop_theme['title'] : null;
				$updated_local_themes[ $key ]['screenshot']           = isset( $shop_theme['image_html'] ) ? $shop_theme['image_html'] : null;
				$updated_local_themes[ $key ]['hasUpdate']            = false;
				$updated_local_themes[ $key ]['hasPackage']           = false;
				$updated_local_themes[ $key ]['author']               = 'pixelgrade';
				$updated_local_themes[ $key ]['actions']['customize'] = false;
				$updated_local_themes[ $key ]['installed']            = false;
				$updated_local_themes[ $key ]['slug']                 = $shop_theme['slug'];
				$updated_local_themes[ $key ]['download_url']         = isset( $shop_theme['download_url'] ) ? $shop_theme['download_url'] : null;
				$updated_local_themes[ $key ]['demo_url']             = isset( $shop_theme['demo_url'] ) ? $shop_theme['demo_url'] : null;
				$updated_local_themes[ $key ]['image_url']            = isset( $shop_theme['image_url'] ) ? $shop_theme['image_url'] : null;
				$updated_local_themes[ $key ]['hash_id']              = isset( $shop_theme['hash_id'] ) ? $shop_theme['hash_id'] : null;
			}
		}

		return $updated_local_themes;
	}

	/**
	 * Main PixelgradeCare_Club Instance
	 *
	 * Ensures only one instance of PixelgradeCare_Club is loaded or can be loaded.
	 *
	 * @since  1.3.0
	 * @static
	 *
	 * @param  object $parent Main PixelgradeCare instance.
	 *
	 * @return object Main PixelgradeCare_Club instance
	 */
	public static function instance( $parent ) {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}

		return self::$_instance;
	} // End instance().


	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {

		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cheatin&#8217; huh?' ) ), esc_html( $this->parent->get_version() ) );
	} // End __clone().

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {

		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cheatin&#8217; huh?' ) ), esc_html( $this->parent->get_version() ) );
	} // End __wakeup().
}
