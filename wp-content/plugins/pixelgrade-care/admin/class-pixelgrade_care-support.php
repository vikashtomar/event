<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    PixelgradeCare
 * @subpackage PixelgradeCare/admin
 * @author     Pixelgrade <email@example.com>
 */
class PixelgradeCare_Support {

	/**
	 * The main plugin object (the parent).
	 * @var     PixelgradeCare
	 * @access  public
	 * @since     1.3.0
	 */
	public $parent = null;

	/**
	 * The only instance.
	 * @var     PixelgradeCareAdmin
	 * @access  protected
	 * @since   1.3.0
	 */
	protected static $_instance = null;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
	}

	function support_setup() {
		// We don't show the Theme Help button and overlay of the current user can't manage options or if we are in the network admin sections on a multisite installation
		if ( ! current_user_can( 'manage_options' ) || is_network_admin() ) {
			return false;
		}

		wp_enqueue_style( 'galanogrotesquealt', '//pxgcdn.com/fonts/galanogrotesquealt/stylesheet.css' );

		wp_enqueue_style( 'galanoclassic', '//pxgcdn.com/fonts/galanoclassic/stylesheet.css' );

		if ( is_rtl() ) {
			wp_enqueue_style( 'pixelgrade_care_style', plugin_dir_url( $this->parent->file ) . 'admin/css/pixelgrade_care-admin-rtl.css', array(), $this->parent->get_version(), 'all' );
		} else {
			wp_enqueue_style( 'pixelgrade_care_style', plugin_dir_url( $this->parent->file ) . 'admin/css/pixelgrade_care-admin.css', array(), $this->parent->get_version(), 'all' );
		}

		wp_enqueue_script( 'pixelgrade_care_support', plugin_dir_url( $this->parent->file ) . 'admin/js/support.js', array(
			'jquery',
			'wp-util',
			'updates'
		), $this->parent->get_version(), true );

		if ( ! wp_script_is('pixelgrade_care-dashboard') ) {
			$this->parent->plugin_admin->localize_js_data( 'pixelgrade_care_support' );
		}

		$this->support_content();
	}

	/**
	 * Output the content for the current step.
	 */
	public function support_content() {
		if ( ! current_user_can( 'manage_options' ) || is_network_admin() ) {
			return false;
		} ?>
		<div id="pixelgrade_care_support_section"></div>
	<?php
	}

	// Determine if the current user has an active theme license and is allowed to use the support section
	private function _has_active_license() {
		$pixcare_option = get_option( 'pixcare_options' );

		if ( ! isset( $pixcare_option['state'] ) && ! isset( $pixcare_option['state']['licenses'] ) ) {
			return false;
		}

		if ( empty( $pixcare_option['state']['licenses'] ) ) {
			return false;
		}

		if ( empty( $pixcare_option['state']['licenses'][0]['license_hash'] ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Main PixelgradeCare_Support Instance
	 *
	 * Ensures only one instance of PixelgradeCare_Support is loaded or can be loaded.
	 *
	 * @since  1.3.0
	 * @static
	 * @param  object $parent Main PixelgradeCare instance.
	 * @return object Main PixelgradeCare_Support instance
	 */
	public static function instance( $parent ) {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance().

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {

		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cheatin&#8217; huh?' ) ), esc_html( $this->parent->get_version() ) );
	} // End __clone().

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {

		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cheatin&#8217; huh?' ) ), esc_html( $this->parent->get_version() ) );
	} // End __wakeup().
}
