<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    PixelgradeCare
 * @subpackage PixelgradeCare/admin
 * @author     Pixelgrade <email@example.com>
 */
class PixelgradeCareAdmin {
	/**
	 * The main plugin object (the parent).
	 * @var     PixelgradeCare
	 * @access  public
	 * @since     1.3.0
	 */
	public $parent = null;

	/**
	 * The config for the active theme.
	 * If this is false it means the current theme doesn't declare support for pixelgrade_care
	 *
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array / boolean    $theme_support
	 */
	private $theme_support;

	private $options;
	private $wp_nonce;
	private $pixcare_nonce;

	private $pixelgrade_care_manager_api_version = 'v2';

	/**
	 * The only instance.
	 * @var     PixelgradeCareAdmin
	 * @access  protected
	 * @since   1.3.0
	 */
	protected static $_instance = null;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;

		add_action( 'admin_init', array( $this, 'check_theme_support' ), 11 );
		add_action( 'rest_api_init', array( $this, 'add_rest_routes_api' ) );
		add_action( 'admin_init', array( $this, 'admin_redirects' ), 15 );
		add_filter( 'wupdates_call_data_request', array( $this, 'add_license_to_wupdates_data' ), 10, 2 );
		add_filter( 'pre_set_site_transient_update_themes', array( $this, 'check_if_update_is_valid' ), 999, 1 );

		add_action( 'admin_notices', array( $this, 'admin_notices' ) );
		add_action( 'tgmpa_init', array( $this, 'pixcare_update_tgmpa_required_plugins' ) );
		include_once( ABSPATH . 'wp-includes/pluggable.php' );

		$this->wp_nonce      = wp_create_nonce( 'wp_rest' );
		$this->pixcare_nonce = wp_create_nonce( 'pixelgrade_care_rest' );
	}

	/**
	 * The first access to PixCare needs to be redirected to the setup wizard
	 */
	function admin_redirects() {
		if ( ! is_admin() || ! current_user_can( 'manage_options' ) ) {
			return;
		}

		$plugin_version     = get_option( 'pixelgrade_care_version' );
		$redirect_transient = get_site_transient( '_pixcare_activation_redirect' );

		if ( $redirect_transient ) {
			// yay this is a fresh install and we are not on a setup page, just go there already
			wp_redirect( admin_url( 'index.php?page=pixelgrade_care-setup-wizard' ) );
			exit;
		} elseif ( empty( $plugin_version ) ) {
			// yay this is a fresh install and we are not on a setup page, just go there already
			wp_redirect( admin_url( 'index.php?page=pixelgrade_care-setup-wizard' ) );
			exit;
		}

		// If the user that is installing pixcare is a member of pixelgrade club (has been given the plugin and no theme)
		// check if the plugin version is empty and has no other pixelgrade theme installed
		if ( empty( $plugin_version ) && ! $this->has_pixelgrade_theme() ) {
			wp_redirect( admin_url( 'index.php?page=pixelgrade_care-setup-wizard' ) );
			exit;
		}
	}

	private function has_pixelgrade_theme() {
		$themes = wp_get_themes();

		// Loop through the themes
		// if we find a theme from pixelgrade return true
		foreach ( $themes as $theme ) {
			$theme_author = $theme->get( 'Author' );
			if ( ! empty( $theme_author ) && strtolower( $theme_author ) == 'pixelgrade' ) {
				return true;
			}
		}

		// No themes from pixelgrade found return false
		return false;
	}

	/**
	 * Pass data to WUpdates which should help validate our theme license and give access to updates.
	 *
	 * @param array $data The optional data that is being passed to WUpdates
	 * @param string $slug The product's slug
	 *
	 * @return array
	 */
	function add_license_to_wupdates_data( $data, $slug ) {
		// We need to make sure that we are adding the license hash to the proper update check
		// Each product fires this filter when it checks for updates; including this very own Pixelgrade Care plugin
		// For now we will only allow it to work for the current theme (we assume only themes require licenses)
		// @todo This DOES NOT WORK if we have plugins with licenses
		if ( $slug == basename( get_template_directory() ) ) {
			$data['license_hash'] = 'pixcare_no_license';

			$license_hash = get_theme_mod( 'pixcare_license_hash' );
			if ( $license_hash ) {
				$data['license_hash'] = $license_hash;
			}
		}

		return $data;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		if ( $this->is_pixelgrade_care_dashboard() ) {
			if ( is_rtl() ) {
				wp_enqueue_style( $this->parent->get_plugin_name(), plugin_dir_url( $this->parent->file ) . 'admin/css/pixelgrade_care-admin-rtl.css', array(), $this->parent->get_version(), 'all' );
			} else {
				wp_enqueue_style( $this->parent->get_plugin_name(), plugin_dir_url( $this->parent->file ) . 'admin/css/pixelgrade_care-admin.css', array(), $this->parent->get_version(), 'all' );
			}
		}
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		if ( $this->is_pixelgrade_care_dashboard() ) {

			wp_enqueue_style( 'galanogrotesquealt', '//pxgcdn.com/fonts/galanogrotesquealt/stylesheet.css' );
			wp_enqueue_style( 'galanoclassic', '//pxgcdn.com/fonts/galanoclassic/stylesheet.css' );

			wp_enqueue_script( 'updates' );

			wp_enqueue_script( 'pixelgrade_care-dashboard', plugin_dir_url( $this->parent->file ) . 'admin/js/dashboard.js', array(
				'jquery',
				'wp-util',
			), $this->parent->get_version(), true );

			$this->localize_js_data();
		}

		if ( isset( $_GET['page'] ) && $_GET['page'] === 'pixelgrade_club' ) {
			wp_enqueue_script( 'pixelgrade_care-club-themes', plugin_dir_url( $this->parent->file ) . 'admin/js/club/club-themes.js', array(
				'jquery',
				'wp-util',
			), $this->parent->get_version(), true );
		}

		// Analytics Code
		$pixcare_options    = get_option( 'pixcare_options' );
		$allow_data_collect = $pixcare_options['allow_data_collect'];

		if ( is_admin() && $allow_data_collect == true ) {
			wp_enqueue_script( 'pixelgrade_care-analytics', plugin_dir_url( $this->parent->file ) . 'admin/js/analytics.js', $this->parent->get_version(), true );
		}
	}

	function add_rest_routes_api() {
		//The Following registers an api route with multiple parameters.
		register_rest_route( 'pixcare/v1', '/global_state', array(
			'methods'             => 'GET',
			'callback'            => array( $this, 'rest_get_state' ),
			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );

		register_rest_route( 'pixcare/v1', '/global_state', array(
			'methods'             => 'POST',
			'callback'            => array( $this, 'rest_set_state' ),
			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );

		register_rest_route( 'pixcare/v1', '/data_collect', array(
			'methods'             => 'GET',
			'callback'            => array( $this, 'rest_get_data_collect' ),
			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );

		register_rest_route( 'pixcare/v1', '/data_collect', array(
			'methods'             => 'POST',
			'callback'            => array( $this, 'rest_set_data_collect' ),
			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );

		// debug tools
		register_rest_route( 'pixcare/v1', '/cleanup', array(
			'methods'             => 'POST',
			'callback'            => array( $this, 'rest_cleanup' ),
			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );

		register_rest_route( 'pixcare/v1', '/disconnect_user', array(
			'methods'             => 'POST',
			'callback'            => array( $this, 'rest_disconnect_user' ),
			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );

		register_rest_route( 'pixcare/v1', '/update_license', array(
			'methods'  => WP_REST_Server::CREATABLE,
			'callback' => array( $this, 'rest_update_license' ),
		) );

		register_rest_route( 'pixcare/v1', '/rest_download_theme', array(
			'methods'             => WP_REST_Server::CREATABLE,
			'callback'            => array( $this, 'rest_download_theme' ),
			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );

		register_rest_route( 'pixcare/v1', '/rest_activate_theme', array(
			'methods'  => WP_REST_Server::CREATABLE,
			'callback' => array( $this, 'rest_activate_theme' ),
//			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );

		register_rest_route( 'pixcare/v1', '/license_info', array(
			'methods'  => WP_REST_Server::READABLE,
			'callback' => array( $this, 'rest_license_info' ),

		) );

		register_rest_route( 'pixcare/v1', '/get_required_plugins', array(
			'methods'             => WP_REST_Server::CREATABLE,
			'callback'            => array( $this, 'get_required_plugins' ),
			'permission_callback' => array( $this, 'permission_nonce_callback' ),
		) );
	}

	/**
	 * @param WP_REST_Request $request
	 *
	 * @return false|int
	 */
	function permission_nonce_callback( $request ) {
		return wp_verify_nonce( $this->get_nonce( $request ), 'pixelgrade_care_rest' );
	}

	/**
	 * @param WP_REST_Request $request
	 *
	 * @return null|string
	 */
	private function get_nonce( $request ) {
		$nonce = null;

		// Get the nonce we've been given
		$nonce = $request->get_param( 'pixcare_nonce' );
		if ( ! empty( $nonce ) ) {
			$nonce = wp_unslash( $nonce );
		}

		return $nonce;
	}

	/**
	 * @param WP_REST_Request $request
	 *
	 * @return array|null|string
	 */
	private function get_request_user_meta( $request ) {
		$data = null;

		$params_data = $request->get_param( 'user' );

		if ( null !== $params_data ) {
			$data = wp_unslash( $params_data );
		}

		return $data;
	}

	/**
	 * @param WP_REST_Request $request
	 *
	 * @return array|null|string
	 */
	private function get_request_theme_mod( $request ) {
		$data = null;

		$params_data = $request->get_param( 'theme_mod' );

		if ( null !== $params_data ) {
			$data = wp_unslash( $params_data );
		}

		return $data;
	}

	/**
	 * @TODO Find a use for this
	 */
	function rest_get_state() {
		$display_errors = @ini_set( 'display_errors', 0 );
		// clear whatever was printed before, we only need a pure json
		if ( ob_get_length() ) {
			ob_get_clean();
		}

		$pixcare_state = $this->get_option( 'state' );

		@ini_set( 'display_errors', $display_errors );
		wp_send_json_success( $pixcare_state );
	}

	/**
	 * Gets the new license and updates it
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return WP_REST_Response
	 */
	function rest_update_license( $request ) {
		$display_errors = @ini_set( 'display_errors', 0 );

		// clear whatever was printed before, we only need a pure json
		if ( ob_get_length() ) {
			ob_get_clean();
		}

		$params = $request->get_params();

		if ( empty( $params['old_license'] ) ) {
			return rest_ensure_response( array( 'success' => false, 'message' => 'No old license provided!' ) );
		}

		if ( empty( $params['new_license'] ) ) {
			return rest_ensure_response( array( 'success' => false, 'message' => 'No new license provided!' ) );
		}

		if ( empty( $params['new_license_status'] ) ) {
			return rest_ensure_response( array( 'success' => false, 'message' => 'No license status provided!' ) );
		}

		if ( empty( $params['new_license_type'] ) ) {
			$params['new_license_type'] = 'shop';
		}

		// Check the old license with the current license. If they're the same - update the license with the new one
		$current_license_hash = get_theme_mod( 'pixcare_license_hash' );

		$set_license        = false;
		$set_license_status = false;
		$set_license_type   = false;
		$set_license_exp    = false;

		if ( $current_license_hash === $params['old_license'] ) {
			$set_license = $params['new_license'];
			set_theme_mod( 'pixcare_license_hash', $params['new_license'] );
			$set_license_status = $params['new_license_status'];
			set_theme_mod( 'pixcare_license_status', $params['new_license_status'] );
			$set_license_type = $params['new_license_type'];
			set_theme_mod( 'pixcare_license_type', $params['new_license_type'] );
			$set_license_exp = $params['pixcare_license_expiry_date'];
			set_theme_mod( 'pixcare_license_expiry_date', $params['pixcare_license_expiry_date'] );
		}

		@ini_set( 'display_errors', $display_errors );

		return rest_ensure_response( array(
			'success'                     => true,
			'updated_license'             => $set_license,
			'updated_license_status'      => $set_license_status,
			'updated_license_type'        => $set_license_type,
			'updated_license_expiry_date' => $set_license_exp
		) );

	}

	/**
	 * Gets the current license info including product details.
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return WP_REST_Response
	 */
	function rest_license_info( $request ) {
		$display_errors = @ini_set( 'display_errors', 0 );

		// clear whatever was printed before, we only need a pure json
		if ( ob_get_length() ) {
			ob_get_clean();
		}

		$params = $request->get_params();

		// These security measures are not actual security, but a way to block bots scanning for endpoints
		// Due to the fact that the data shared is not sensitive, we consider it enough

		// If the dirty little secret is missing or wrong, no need to bother.
		if ( empty( $params['dirtysecret'] ) && 'QH5xX30DeLlq5tyIhM53749bk72Bn3Mfi7UR' !== $params['dirtysecret'] ) {
			return rest_ensure_response( array( 'success' => false, 'message' => 'You are wrong, dirty you!' ) );
		}

		// Limit the origin to pixelgrade.com
		$origin = $request->get_header( 'origin' );
		if ( empty( $origin ) && PIXELGRADE_CARE__API_BASE_DOMAIN !== $origin ) {
			return rest_ensure_response( array( 'success' => false, 'message' => 'No no! Move along.' ) );
		}

		// Double check with the user agent
		$user_agent = $request->get_header( 'user-agent' );
		if ( empty( $user_agent ) && false === strpos( $user_agent, PIXELGRADE_CARE__API_BASE_DOMAIN ) ) {
			return rest_ensure_response( array( 'success' => false, 'message' => 'No no! Move along please.' ) );
		}

		/**
		 * Lets start gathering the license info
		 */
		$data = array(
			'license' => array(),
			'theme'   => array(),
			'users'   => array(),
			'site'    => array(),
		);

		/**
		 * Get the license info
		 */
		$data['license']['hash']        = get_theme_mod( 'pixcare_license_hash' );
		$data['license']['status']      = get_theme_mod( 'pixcare_license_status' );
		$data['license']['type']        = get_theme_mod( 'pixcare_license_type' );
		$data['license']['expiry_date'] = get_theme_mod( 'pixcare_license_expiry_date' );

		/**
		 * Get the theme's stylesheet header details and add them to the list
		 */
		$current_theme = wp_get_theme();

		$data['theme']['stylesheet'] = array(
			'Name'        => $current_theme->get( 'Name' ),
			'ThemeURI'    => $current_theme->get( 'ThemeURI' ),
			'Description' => $current_theme->get( 'Description' ),
			'Author'      => $current_theme->get( 'Author' ),
			'AuthorURI'   => $current_theme->get( 'AuthorURI' ),
			'Version'     => $current_theme->get( 'Version' ),
			'Template'    => $current_theme->get( 'Template' ),
			'Status'      => $current_theme->get( 'Status' ),
			'Tags'        => $current_theme->get( 'Tags' ),
			'TextDomain'  => $current_theme->get( 'TextDomain' ),
			'DomainPath'  => $current_theme->get( 'DomainPath' ),
		);

		// Try and get the current (parent) theme  WUpdates info
		$slug         = basename( get_template_directory() );
		$wupdates_ids = apply_filters( 'wupdates_gather_ids', array() );
		if ( $this->is_wupdates_filter_unchanged() ) {
			if ( ! empty( $wupdates_ids[ $slug ] ) ) {
				$data['theme']['wupdates'] = $wupdates_ids[ $slug ];
			}
		}

		// Get the theme roots
		$data['theme']['roots'] = get_theme_roots();
		// Get the current (parent) theme directory URI
		$data['theme']['directory_uri'] = get_parent_theme_file_uri();

		// Get the current (parent) theme stylesheet URI
		$data['theme']['stylesheet_uri'] = get_parent_theme_file_uri( 'style.css' );

		/**
		 * Some user information
		 */
		// Find users that have the PixCare meta connect info
		$users = get_users( array(
			'meta_key' => 'pixelgrade_user_email',
		) );

		if ( ! empty( $users ) ) {
			/** @var WP_User $user */
			foreach ( $users as $user ) {
				$user_meta = get_user_meta( $user->ID );
				$user_data = array();
				if ( ! empty( $user_meta['pixcare_user_ID'] ) ) {
					$user_data['pixelgrade_user_id'] = (int) reset( $user_meta['pixcare_user_ID'] );
				}

				if ( ! empty( $user_meta['pixelgrade_user_login'] ) ) {
					$user_data['pixelgrade_user_login'] = (string) reset( $user_meta['pixelgrade_user_login'] );
				}

				if ( ! empty( $user_meta['pixelgrade_user_email'] ) ) {
					$user_data['pixelgrade_user_email'] = (string) reset( $user_meta['pixelgrade_user_email'] );
				}

				if ( ! empty( $user_data ) ) {
					$data['users'][ $user->ID ] = $user_data;
				}
			}
		}

		/**
		 * Some installation information
		 */
		$data['site']['is_ssl']       = is_ssl();
		$data['site']['is_multisite'] = is_multisite();

		/** @var PixelgradeCare $local_plugin */
		$local_plugin                    = PixelgradeCare();
		$data['site']['pixcare_version'] = $local_plugin->get_version();

		@ini_set( 'display_errors', $display_errors );

		return rest_ensure_response( $data );

	}

	/**
	 * Helper function that gets the value of allow_data_collect option
	 */
	function rest_get_data_collect() {
		$display_errors = @ini_set( 'display_errors', 0 );
		// clear whatever was printed before, we only need a pure json
		if ( ob_get_length() ) {
			ob_get_clean();
		}

		$pcoptions          = get_option( 'pixcare_options' );
		$allow_data_collect = $pcoptions['allow_data_collect'];

		wp_send_json( $allow_data_collect );
	}

	/**
	 * Helper function that sets the value of allow_data_collect option
	 *
	 * @param WP_REST_Request $request
	 */
	function rest_set_data_collect( $request ) {
		$display_errors = @ini_set( 'display_errors', 0 );
		// clear whatever was printed before, we only need a pure json
		if ( ob_get_length() ) {
			ob_get_clean();
		}

		$params           = $request->get_params();
		$has_data_collect = $params['allow_data_collect'];

		if ( ! isset( $has_data_collect ) ) {
			wp_send_json( 'Something went wrong. No arguments.' );
		}
		$this->set_options();

		$this->options['allow_data_collect'] = $params['allow_data_collect'];
		$this->save_options();

		wp_send_json( $this->options['allow_data_collect'] );
	}

	/**
	 * @param  WP_REST_Request $request
	 */
	function rest_set_state( $request ) {
		$display_errors = @ini_set( 'display_errors', 0 );
		// clear whatever was printed before, we only need a pure json
		if ( ob_get_length() ) {
			ob_get_clean();
		}

		$user_data  = $this->get_request_user_meta( $request );
		$theme_data = $this->get_request_theme_mod( $request );

		if ( ! empty( $user_data ) && is_array( $user_data ) ) {

			$current_user = self::get_theme_activation_user();

			if ( isset( $user_data['oauth_token'] ) ) {
				update_user_meta( $current_user->ID, 'pixcare_oauth_token', $user_data['oauth_token'] );
			}

			if ( isset( $user_data['pixelgrade_user_ID'] ) ) {
				update_user_meta( $current_user->ID, 'pixcare_user_ID', $user_data['pixelgrade_user_ID'] );
			}

			if ( isset( $user_data['pixelgrade_user_login'] ) ) {
				// Make sure that we have encoded characters in proper form
				$user_data['pixelgrade_user_login'] = str_replace( array( '+', '%7E' ), array(
					' ',
					'~'
				), $user_data['pixelgrade_user_login'] );
				update_user_meta( $current_user->ID, 'pixelgrade_user_login', $user_data['pixelgrade_user_login'] );
			}

			if ( isset( $user_data['pixelgrade_user_email'] ) ) {
				update_user_meta( $current_user->ID, 'pixelgrade_user_email', $user_data['pixelgrade_user_email'] );
			}

			if ( isset( $user_data['pixelgrade_display_name'] ) ) {
				// Make sure that we have encoded characters in proper form
				$user_data['pixelgrade_display_name'] = str_replace( array( '+', '%7E' ), array(
					' ',
					'~'
				), $user_data['pixelgrade_display_name'] );
				update_user_meta( $current_user->ID, 'pixelgrade_display_name', $user_data['pixelgrade_display_name'] );
			}

			if ( isset( $user_data['oauth_token_secret'] ) ) {
				update_user_meta( $current_user->ID, 'pixcare_oauth_token_secret', $user_data['oauth_token_secret'] );
			}

			if ( isset( $user_data['oauth_verifier'] ) ) {
				update_user_meta( $current_user->ID, 'pixcare_oauth_verifier', $user_data['oauth_verifier'] );
			}

			// Update the available Pixelgrade Themes
			if ( isset( $user_data['pixelgrade_themes'] ) ) {
				update_user_meta( $current_user->ID, 'pixelgrade_themes', $user_data['pixelgrade_themes'] );
			}
		}

		if ( ! empty( $theme_data ) && is_array( $theme_data ) ) {

			if ( isset( $theme_data['license_hash'] ) ) {
				// We have received a new license hash
				// Before we update the theme mod, we need to see if this is different than the one currently in use
				$current_theme_license_hash = get_theme_mod( 'pixcare_license_hash' );
				if ( $current_theme_license_hash != $theme_data['license_hash'] ) {
					// We have received a new license(_hash)
					// We need to force a theme update check because with the new license we might have access to updates
					delete_site_transient( 'update_themes' );
					// Also delete our own saved data
					remove_theme_mod( 'pixcare_new_theme_version' );
				}

				set_theme_mod( 'pixcare_license_hash', $theme_data['license_hash'] );
			}

			if ( isset( $theme_data['status'] ) ) {
				set_theme_mod( 'pixcare_license_status', $theme_data['status'] );
			}

			if ( isset( $theme_data['license_type'] ) ) {
				set_theme_mod( 'pixcare_license_type', $theme_data['license_type'] );
			}

			if ( isset( $theme_data['license_exp'] ) ) {
				set_theme_mod( 'pixcare_license_expiry_date', $theme_data['license_exp'] );
			}
		}

		if ( ! empty( $_POST['option'] ) && isset( $_POST['value'] ) ) {
			$option = wp_unslash( $_POST['option'] );
			$value  = wp_unslash( $_POST['value'] );

			$this->options[ $option ] = $value;
			$this->save_options();

			rest_ensure_response( 1 );
		}
	}

	function rest_cleanup() {
		$display_errors = @ini_set( 'display_errors', 0 );
		// clear whatever was printed before, we only need a pure json
		if ( ob_get_length() ) {
			ob_get_clean();
		}

		if ( empty( $_POST['test1'] ) || empty( $_POST['test2'] ) || empty( $_POST['confirm'] ) ) {
			wp_send_json_error( 'nah' );
		}

		if ( (int) $_POST['test1'] + (int) $_POST['test2'] === (int) $_POST['confirm'] ) {
			$current_user = self::get_theme_activation_user();

			delete_user_meta( $current_user->ID, 'pixcare_oauth_token' );
			delete_user_meta( $current_user->ID, 'pixcare_oauth_token_secret' );
			delete_user_meta( $current_user->ID, 'pixcare_oauth_verifier' );
			delete_user_meta( $current_user->ID, 'pixcare_user_ID' );
			delete_user_meta( $current_user->ID, 'pixelgrade_user_login' );
			delete_user_meta( $current_user->ID, 'pixelgrade_user_email' );
			delete_user_meta( $current_user->ID, 'pixelgrade_display_name' );

			remove_theme_mod( 'pixcare_theme_config' );
			remove_theme_mod( 'pixcare_license_hash' );
			remove_theme_mod( 'pixcare_license_status' );
			remove_theme_mod( 'pixcare_license_type' );
			remove_theme_mod( 'pixcare_license_expiry_date' );

			// We will also clear the theme update transient because when one reconnects it might use a different license
			// and that license might allow for updates
			// Right now we prevent the update package URL to be saved in the transient (via the WUpdates code)
			delete_site_transient( 'update_themes' );
			// Also delete our own saved data
			remove_theme_mod( 'pixcare_new_theme_version' );

			delete_option( 'pixcare_options' );

			wp_send_json_success( 'ok' );
		}

		wp_send_json_error( array(
			$_POST['test1'],
			$_POST['test2'],
			$_POST['confirm']
		) );
	}

	function rest_disconnect_user() {
		$display_errors = @ini_set( 'display_errors', 0 );
		// clear whatever was printed before, we only need a pure json
		if ( ob_get_length() ) {
			ob_get_clean();
		}

		if ( empty( $_POST['user_id'] ) ) {
			wp_send_json_error( 'no user?' );
		}

		$user_id = $_POST['user_id'];

		// We will remove the connection details for the user that has actually connected and activated
		$current_user = self::get_theme_activation_user();

		// We will ping pixelgrade.com to deactivate the activation
		$license_hash = get_theme_mod( 'pixcare_license_hash' );
		if ( ! empty( $license_hash ) ) {
			$deactivate_license_url = trailingslashit( PIXELGRADE_CARE__API_BASE ) . 'wp-json/wupl/v1/front/license_action';

			// Get all kind of details about the active theme
			$theme_details = $this->get_theme_support();

			$data = array(
				'action'       => 'deactivate',
				'license_hash' => $license_hash,
				'site_url'     => home_url( '/' ),
				'is_ssl'       => is_ssl(),
			);

			if ( isset( $theme_details['theme_version'] ) ) {
				$data['current_version'] = $theme_details['theme_version'];
			}

			// We will do a non blocking request
			wp_remote_post( $deactivate_license_url,
				array(
					'timeout'   => 10,
					'blocking'  => false, // We don't care about the response so don't use blocking requests
					'body'      => $data,
					'sslverify' => false,
				) );
		}

//		if ( (int) $user_id === $current_user->ID ) {
		delete_user_meta( $current_user->ID, 'pixcare_oauth_token' );
		delete_user_meta( $current_user->ID, 'pixcare_oauth_token_secret' );
		delete_user_meta( $current_user->ID, 'pixcare_oauth_verifier' );
		delete_user_meta( $current_user->ID, 'pixcare_user_ID' );
		delete_user_meta( $current_user->ID, 'pixelgrade_user_login' );
		delete_user_meta( $current_user->ID, 'pixelgrade_user_email' );
		delete_user_meta( $current_user->ID, 'pixelgrade_display_name' );

		remove_theme_mod( 'pixcare_license_hash' );
		remove_theme_mod( 'pixcare_license_status' );
		remove_theme_mod( 'pixcare_license_type' );
		remove_theme_mod( 'pixcare_license_expiry_date' );

		// Remove the option that stores the themes this user has access to
		delete_user_meta( $current_user->ID, 'pixelgrade_themes' );

		// We will also clear the theme update transient because when one reconnects it might use a different license
		// and that license might allow for updates
		// Right now we prevent the update package URL to be saved in the transient (via the WUpdates code)
		delete_site_transient( 'update_themes' );
		// Also delete our own saved data
		remove_theme_mod( 'pixcare_new_theme_version' );

		wp_send_json_success( 'ok' );
//		}

		wp_send_json_error( 'You cannot disconnect someone else!' );
	}

	/**
	 * An endpoint that will download a pixelgrade club theme
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return WP_REST_Response
	 */
	function rest_download_theme( $request ) {

		include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' ); //for themes_api..
		include_once( ABSPATH . 'wp-admin/includes/misc.php' ); //for themes_api..
		include_once( ABSPATH . 'wp-admin/includes/file.php' ); //for themes_api..
		include_once( ABSPATH . 'wp-admin/includes/class-theme-upgrader.php' ); //for themes_api..
		include_once( ABSPATH . 'wp-admin/includes/class-theme-installer-skin.php' ); //for themes_api..
		include_once( ABSPATH . 'wp-admin/includes/class-automatic-upgrader-skin.php' );
		include_once( ABSPATH . 'wp-admin/includes/file.php' );

		// Try to download and install the theme
		$skin      = new WP_Ajax_Upgrader_Skin();
		$upgrader  = new Theme_Upgrader( $skin );
		$installer = $upgrader->install( $request['download_url'] );

		// In case of error return an unsuccessful message
		if ( ! $installer ) {
			// Check if errors are found. If we have errors - add them to the errors array
			$errors = array();
			if ( property_exists( $upgrader, 'skin' ) && property_exists( $upgrader->skin, 'errors' ) ) {
				$errors = $upgrader->skin->get_errors();
			}

			return rest_ensure_response( array(
				'success' => false,
				'errors'  => $errors
			) );
		}

		// Success
		return rest_ensure_response( array(
			'success'   => true,
			'extracted' => $installer
		) );
	}

	/**
	 * @param WP_REST_Request $request
	 *
	 * @return mixed|WP_REST_Response
	 */
	function rest_activate_theme( $request ) {
		if ( ! isset( $request['slug'] ) ) {
			return rest_ensure_response( 'No theme specified!' );
		}

		// @TODO Set theme to 'installed'
		$current_theme      = wp_get_theme();
		$current_theme_slug = $current_theme->get_stylesheet();

		$themes = $this->get_user_themes();

		// Check if current theme is in the Pixelgrade Club
		if ( $themes && array_key_exists( $current_theme_slug, $themes ) ) {
			$themes[ $current_theme_slug ]['active'] = false;
		}

		// Check if the future new theme is in the Pixelgrade Club
		if ( $themes && array_key_exists( $request['slug'], $themes ) ) {
			$themes[ $request['slug'] ]['active'] = true;
		}

		switch_theme( $request['slug'] );

		$new_theme = wp_get_theme();

		if ( $new_theme->get_stylesheet() == $request['slug'] ) {
			$this->set_user_themes( $themes );
			$required_plugins = array();

			return rest_ensure_response( array(
				'success'         => true,
				'requiredPlugins' => $required_plugins
			) );
		}

		return rest_ensure_response( array( 'success' => false ) );

	}

	// dummy
	function get_required_plugins() {
		$plugins = $this->localize_tgmpa_data();

		remove_filter( 'tgmpa_load', '__return_true' );

		return $plugins;
	}

	function check_theme_support() {
		if ( ! current_theme_supports( 'pixelgrade_care' ) ) {
			return false;
		}
		$config = get_theme_support( 'pixelgrade_care' );

		if ( ! is_array( $config ) ) {
			return false;
		}

		$config = $this->validate_theme_supports( $config[0] );

		if ( ! $config ) {
			return false;
		}
		$this->theme_support = $config;
	}

	function get_theme_support() {
		if ( empty( $this->theme_support ) ) {
			$this->check_theme_support();
		}

		return $this->theme_support;
	}

	function add_pixelgrade_menu() {
		// First determine if we should show a "Heads Up" bubble next to the main  admin menu item
		// We will show it when the license is expired, not connected or activated
		$show_bubble = false;

		// If the theme directory has been changed, show bubble
		if ( ! $this->get_theme_checks() ) {
			$show_bubble = true;
		}

		// This means we are not connected
		$current_user          = self::get_theme_activation_user();
		$pixelgrade_user_login = get_user_meta( $current_user->ID, 'pixelgrade_user_login', true );
		if ( empty( $pixelgrade_user_login ) ) {
			$show_bubble = true;
		}

		// Show bubble if the license is expired
		$license_status = get_theme_mod( 'pixcare_license_status' );
		if ( empty( $license_status ) || in_array( $license_status, array( 'expired' ) ) ) {
			$show_bubble = true;
		}

		// Show bubble if we have an update notification
		$new_theme_version = get_theme_mod( 'pixcare_new_theme_version' );
		$theme_support     = $this->get_theme_support();
		if ( ! empty( $new_theme_version ) && ! empty( $theme_support['theme_version'] ) && version_compare( $theme_support['theme_version'], $new_theme_version, '<' ) ) {
			$show_bubble = true;
		}

		$bubble_markup = '';
		if ( $show_bubble ) {
			$bubble_markup = ' <span class="awaiting-mod"><span class="pending-count">!!︎</span></span>';
		}

		add_menu_page( 'Pixelgrade', 'Pixelgrade' . $bubble_markup, 'install_themes', 'pixelgrade_care', array(
			$this,
			'pixelgrade_care_options_page'
		), plugin_dir_url( 'pixelgrade-care/admin/images/pixelgrade-menu-image.svg' ) . 'pixelgrade-menu-image.svg', 2 );

		add_submenu_page( 'pixelgrade_care', 'Dashboard', 'Dashboard', 'manage_options', 'pixelgrade_care', array(
			$this,
			'pixelgrade_care_options_page'
		) );

		// Add the themes page
		if ( ! empty( $pixelgrade_user_login ) ) {
			add_submenu_page( 'pixelgrade_care', 'Pixelgrade Themes', 'Themes', 'manage_options', 'pixelgrade_club', array(
				$this,
				'club_themes_template'
			) );
		}

	}

	/**
	 * This function will render the layout for the Pixelgrade Theme pages.
	 * Renders the club-page template - defined in the /templates folder
	 */
	function club_themes_template() {
		require_once plugin_dir_path( $this->parent->file ) . 'admin/templates/pixelgrade-club-page.php';

		get_pixelgrade_club_page_layout( $this->parent->file );
	}

	function localize_js_data( $key = 'pixelgrade_care-dashboard' ) {
		if ( empty( $this->theme_support ) ) {
			$this->check_theme_support();
		}

		$current_user = self::get_theme_activation_user();

		$theme_config = $this->get_config();

		if ( class_exists( 'TGM_Plugin_Activation' ) ) {
			$theme_config['pluginManager']['tgmpaPlugins'] = $this->localize_tgmpa_data();
		}

		// Use camelCase since this is going to JS!!!
		$localized_data = array(
			'apiBase'        => PIXELGRADE_CARE__API_BASE,
			'apiBaseDomain'  => PIXELGRADE_CARE__API_BASE_DOMAIN,
			'shopBase'       => PIXELGRADE_CARE__SHOP_BASE,
			'shopBaseDomain' => PIXELGRADE_CARE__SHOP_BASE_DOMAIN,
			'devMode'        => PIXELGRADE_CARE__DEV_MODE,

			'themeSupports' => $this->theme_support,
			'themeConfig'   => $theme_config,
			'wpRest'        => array(
				'root'          => esc_url_raw( rest_url() ),
				'base'          => esc_url_raw( rest_url() . 'pixcare/v1/' ),
				'endpoint'      => array(
					'globalState'        => esc_url_raw( rest_url() . 'pixcare/v1/global_state' ),
					'dataCollect'        => esc_url_raw( rest_url() . 'pixcare/v1/data_collect' ),
					'cleanup'            => esc_url_raw( rest_url() . 'pixcare/v1/cleanup' ),
					'disconnectUser'     => esc_url_raw( rest_url() . 'pixcare/v1/disconnect_user' ),
					'updateLicense'      => esc_url_raw( rest_url() . 'pixcare/v1/update_license' ),
					'licenseInfo'        => esc_url_raw( rest_url() . 'pixcare/v1/license_info' ),
					'import'             => esc_url_raw( rest_url() . 'pixcare/v1/import' ),
					'uploadMedia'        => esc_url_raw( rest_url() . 'pixcare/v1/upload_media' ),
					'downloadTheme'      => esc_url_raw( rest_url() . 'pixcare/v1/rest_download_theme' ),
					'activateTheme'      => esc_url_raw( rest_url() . 'pixcare/v1/rest_activate_theme' ),
					'getRequiredPlugins' => esc_url_raw( rest_url() . 'pixcare/v1/get_required_plugins' ),
				),
				'nonce'         => $this->wp_nonce,
				'pixcare_nonce' => $this->pixcare_nonce,
			),
			// why is this a global prop?
			'systemStatus'  => array(
				'allowCollectData' => $this->get_option( 'allow_data_collect', true ),
				'installData'      => $this->get_install_data(),
			),
			'knowledgeBase' => get_option( 'pixcare_support_' . $this->get_original_theme_slug() ),
			'siteUrl'       => home_url( '/' ),
			'dashboardUrl'  => admin_url( 'admin.php?page=pixelgrade_care' ),
			'adminUrl'      => admin_url(),
			'themesUrl'     => admin_url( 'themes.php' ),
			'customizerUrl' => admin_url( 'customize.php' ),
			'user'          => array(
				'name'   => ( empty( $current_user->display_name ) ? $current_user->user_login : $current_user->display_name ),
				'id'     => $current_user->ID,
				'email'  => $current_user->user_email,
				'themes' => array()
			),
			'themeMod'      => array(),
			'version'       => $this->parent->get_version(),
		);

		// user data
		$oauth_token = get_user_meta( $current_user->ID, 'pixcare_oauth_token', true );
		if ( ! empty( $oauth_token ) ) {
			$localized_data['user']['oauth_token'] = $oauth_token;
		}

		$oauth_token_secret = get_user_meta( $current_user->ID, 'pixcare_oauth_token_secret', true );
		if ( ! empty( $oauth_token_secret ) ) {
			$localized_data['user']['oauth_token_secret'] = $oauth_token_secret;
		}

		$oauth_verifier = get_user_meta( $current_user->ID, 'pixcare_oauth_verifier', true );
		if ( ! empty( $oauth_verifier ) ) {
			$localized_data['user']['oauth_verifier'] = $oauth_verifier;
		}

		$pixcare_user_ID = get_user_meta( $current_user->ID, 'pixcare_user_ID', true );
		if ( ! empty( $pixcare_user_ID ) ) {
			$localized_data['user']['pixcare_user_ID'] = $pixcare_user_ID;
		}

		$pixelgrade_user_login = get_user_meta( $current_user->ID, 'pixelgrade_user_login', true );
		if ( ! empty( $pixelgrade_user_login ) ) {
			$localized_data['user']['pixelgrade_user_login'] = $pixelgrade_user_login;
		}

		$pixelgrade_user_email = get_user_meta( $current_user->ID, 'pixelgrade_user_email', true );
		if ( ! empty( $pixelgrade_user_email ) ) {
			$localized_data['user']['pixelgrade_user_email'] = $pixelgrade_user_email;
		}

		$pixelgrade_display_name = get_user_meta( $current_user->ID, 'pixelgrade_display_name', true );
		if ( ! empty( $pixelgrade_user_email ) ) {
			$localized_data['user']['pixelgrade_display_name'] = $pixelgrade_display_name;
		}

		// theme data
		// first get the wupdates theme id
		$localized_data['themeSupports']['theme_id'] = $this->get_theme_hash_id();

		// Set a flag that tells the UI if the theme's details (name or directory) have been changed
		$localized_data['themeSupports']['is_original_theme'] = $this->get_theme_checks();

		// Get Original Theme Slug
		$localized_data['themeSupports']['original_slug'] = $this->get_original_theme_slug();

		$license_hash = get_theme_mod( 'pixcare_license_hash' );
		if ( ! empty( $license_hash ) ) {
			$localized_data['themeMod']['licenseHash'] = $license_hash;
		}

		$license_status = get_theme_mod( 'pixcare_license_status' );

		if ( ! empty( $license_status ) ) {
			$localized_data['themeMod']['licenseStatus'] = $license_status;
		}

		// localize the license type - can be either shop or envato
		$license_type = get_theme_mod( 'pixcare_license_type' );

		if ( ! empty( $license_type ) ) {
			$localized_data['themeMod']['licenseType'] = $license_type;
		}

		// localize the license expiry date
		$license_exp = get_theme_mod( 'pixcare_license_expiry_date' );
		if ( ! empty( $license_exp ) ) {
			$localized_data['themeMod']['licenseExpiryDate'] = $license_exp;
		}

		$new_theme_version = get_theme_mod( 'pixcare_new_theme_version' );
		if ( ! empty( $new_theme_version ) ) {
			$localized_data['themeMod']['themeNewVersion'] = $new_theme_version;
		}

		//If we have pixelgrade themes saved - localize those as well
//		$pixelgrade_themes = get_user_meta( $current_user->ID, 'pixelgrade_themes', true );
//		if ( ! empty( $pixelgrade_themes ) ) {
//			$localized_data['user']['themes'] = $pixelgrade_themes;
//		}

		$localized_data = apply_filters( 'pixcare_localized_data', $localized_data );

		wp_localize_script( $key, 'pixcare', $localized_data );
	}

	/**
	 * @return mixed
	 */
	static function localize_tgmpa_data() {
		/** @var TGM_Plugin_Activation $tgmpa */
		global $tgmpa;

		// Bail if we have nothing to work with
		if ( empty( $tgmpa ) || empty( $tgmpa->plugins ) ) {
			return array();
		}

		foreach ( $tgmpa->plugins as $slug => $plugin ) {

			// do not add pixelgrade care in the required plugins array
			if ( $slug === 'pixelgrade-care' ) {
				unset( $tgmpa->plugins[ $slug ] );
				continue;
			}

			$tgmpa->plugins[ $slug ]['is_installed']  = false;
			$tgmpa->plugins[ $slug ]['is_active']     = false;
			$tgmpa->plugins[ $slug ]['is_up_to_date'] = true;
			// We need to test for method existance because older version of TGMPA don't have it
			if ( method_exists( $tgmpa, 'is_plugin_installed' ) && $tgmpa->is_plugin_installed( $slug ) ) {
				$tgmpa->plugins[ $slug ]['is_installed'] = true;

				if ( method_exists( $tgmpa, 'is_plugin_active' ) && $tgmpa->is_plugin_active( $slug ) ) {
					$tgmpa->plugins[ $slug ]['is_active'] = true;
				}

				if ( method_exists( $tgmpa, 'does_plugin_have_update' ) && $tgmpa->does_plugin_have_update( $slug ) ) {
					$tgmpa->plugins[ $slug ]['is_up_to_date'] = false;
				}

				$data = get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin['file_path'], false );

				$tgmpa->plugins[ $slug ]['description']    = $data['Description'];
				$tgmpa->plugins[ $slug ]['active_version'] = $data['Version'];
			}
			$perm = current_user_can( 'activate_plugins' );

			if ( current_user_can( 'activate_plugins' ) && is_plugin_inactive( $plugin['file_path'] ) && method_exists( $tgmpa, 'get_tgmpa_url' ) ) {
				$tgmpa->plugins[ $slug ]['activate_url'] = wp_nonce_url(
					add_query_arg(
						array(
							'plugin'         => urlencode( $slug ),
							'tgmpa-activate' => 'activate-plugin',
						),
						$tgmpa->get_tgmpa_url()
					),
					'tgmpa-activate',
					'tgmpa-nonce'
				);

				$tgmpa->plugins[ $slug ]['install_url'] = wp_nonce_url(
					add_query_arg(
						array(
							'plugin'        => urlencode( $slug ),
							'tgmpa-install' => 'install-plugin',
						),
						$tgmpa->get_tgmpa_url()
					),
					'tgmpa-install',
					'tgmpa-nonce'
				);
			}
		}

		return $tgmpa->plugins;
	}

	/**
	 * Add Contextual help tabs.
	 */
	public function add_tabs() {
		$screen = get_current_screen();

		$screen->add_help_tab( array(
			'id'      => 'pixelgrade_care_setup_wizard_tab',
			'title'   => __( 'Pixelgrade Care Setup', 'pixelgrade_care' ),
			'content' =>
				'<h2>' . __( 'Pixelgrade Care Setup', 'pixelgrade_care' ) . '</h2>' .
				'<p><a href="' . esc_url( admin_url( 'index.php?page=pixelgrade_care-setup-wizard' ) ) . '" class="button button-primary">' . esc_html__( 'Setup Pixelgrade Care', 'pixelgrade_care' ) . '</a></p>'
		) );
	}

	function settings_init() {
		register_setting( 'pixelgrade_care', 'pixelgrade_care_settings' );

		add_settings_section(
			'pixelgrade_care_section',
			esc_html__( 'Pixelgrade Care description', 'pixelgrade_care' ),
			null,
			'pixelgrade_care'
		);
	}

	function pixelgrade_care_settings_section_callback() {
		echo esc_html__( 'This section description', 'pixelgrade_care' );
	}

	function pixelgrade_care_options_page() { ?>
		<div class="pixelgrade_care-wrapper">
			<div id="pixelgrade_care_dashboard"></div>
		</div>
		<?php
	}

	/**
	 * Prepare the theme mods which should hold content
	 *
	 * @param array $value The current value being set up in theme mod
	 * @param array $oldvalue The last known value for this theme mod
	 *
	 * @since    1.2.5
	 * @return array
	 */
	function sanitize_theme_mods_holding_content( $value, $oldvalue ) {
		// Make sure that $value is an array
		if ( ! is_array( $value ) ) {
			$value = array( $value );
		}

		$value = array_map( array( $this, 'sanitize_array_items_for_emojies' ), $value );

		return $value;
	}

	/** === HELPERS=== */

	function validate_theme_supports( $config ) {

		if ( ! empty( $config['support_url'] ) && ! wp_http_validate_url( $config['support_url'] ) ) {
			unset( $config['support_url'] );
		}

		if ( empty( $config['ock'] ) ) {
			$config['ock'] = 'Lm12n034gL19';
		}

		if ( empty( $config['ocs'] ) ) {
			$config['ocs'] = '6AU8WKBK1yZRDerL57ObzDPM7SGWRp21Csi5Ti5LdVNG9MbP';
		}

		if ( ! empty( $config['support_url'] ) && ! wp_http_validate_url( $config['support_url'] ) ) {
			unset( $config['support_url'] );
		}

		if ( empty( $config['onboarding'] ) ) {
			$config['onboarding'] = 1;
		}

		if ( empty( $config['market'] ) ) {
			$config['market'] = 'pixelgrade';
		}

		// Detect wether the current active theme is one of ours
		if ( empty( $config['is_pixelgrade_theme'] ) ) {
			$config['is_pixelgrade_theme'] = $this->is_pixelgrade_theme();
		}

		// Complete the config with theme details
		$theme = wp_get_theme();

		$parent = $theme->parent();

		if ( is_child_theme() && ! empty( $parent ) ) {
			$theme = $parent;
		}

		if ( empty( $config['theme_name'] ) ) {
			$config['theme_name'] = $theme->get( 'Name' );
		}

		if ( empty( $config['theme_uri'] ) ) {
			$config['theme_uri'] = $theme->get( 'ThemeURI' );
		}

		if ( empty( $config['theme_desc'] ) ) {
			$config['theme_desc'] = $theme->get( 'Description' );
		}

		if ( empty( $config['theme_version'] ) ) {
			$config['theme_version'] = $theme->get( 'Version' );
		}

		// THis might not be needed anymore since we have apiBase and the like
		if ( empty( $config['shop_url'] ) ) {
			// the url of the mother shop, trailing slash is required
			$config['shop_url'] = trailingslashit( apply_filters( 'pixelgrade_care_shop_url', PIXELGRADE_CARE__API_BASE ) );
		}

		$config['is_child'] = is_child_theme();

		$config['template'] = $theme->template;

		return apply_filters( 'pixcare_validate_theme_supports', $config );
	}

	function is_pixelgrade_care_dashboard() {
		if ( ! empty( $_GET['page'] ) && 'pixelgrade_care' === $_GET['page'] ) {
			return true;
		}

		return false;
	}

	function set_options() {
		$this->options = get_option( 'pixcare_options' );
	}

	function save_options() {
		update_option( 'pixcare_options', $this->options );
	}

	function get_options() {
		if ( empty( $this->options ) ) {
			$this->set_options();
		}

		return $this->options;
	}

	function get_option( $option, $default = null ) {
		$options = $this->get_options();

		if ( ! empty( $options[ $option ] ) ) {
			return $options[ $option ];
		}

		if ( $default !== null ) {
			return $default;
		}

		return null;
	}

	function get_state_option( $option, $default = null ) {

		$pixcare_state = $this->get_option( 'state' );

		if ( empty( $pixcare_state ) ) {
//			$this->set_default_state_option();
		}

		if ( ! empty( $pixcare_state[ $option ] ) ) {
			return $pixcare_state[ $option ];
		}

		if ( $default !== null ) {
			return $default;
		}

		return null;
	}

	/**
	 * If $content is a string the function will convert any 4 byte emoji in a string to their equivalent HTML entity.
	 * In case that $content is array, it will apply the same rule recursively on each array item
	 *
	 * @param array|string $content
	 *
	 * @since 1.2.5
	 * @return array|string
	 */
	private function sanitize_array_items_for_emojies( $content ) {
		if ( is_string( $content ) ) {
			return wp_encode_emoji( $content );
		} elseif ( is_array( $content ) ) {
			foreach ( $content as $key => $item ) {
				$content[ $key ] = $this->sanitize_array_items_for_emojies( $item );
			}

			return $content;
		}

		return $content;
	}

	function init_knowledgeBase_categories() {
		$support_mod = get_option( 'pixcare_support_' . $this->get_original_theme_slug() );

		if ( empty( $support_mod ) ) {
			$this->_set_knowledgeBase_categories();
		}
	}

	function transient_update_kb_categories( $transient ) {
		$this->_set_knowledgeBase_categories();

		return $transient;
	}

	public function _set_knowledgeBase_categories() {
		// Get default
		$knowledge_base = array(
			'categories' => $this->_get_kb_categories()
		);

		// Save to theme mod
		update_option( 'pixcare_support_' . $this->get_original_theme_slug(), $knowledge_base );
	}

	public function _get_kb_categories() {
		// Get existing categories
		$args = array(
			'timeout' => 20
		);

		// Initiate the slug of the THEME that we will get the kbs for
		$slug = ( ! isset( $this->theme_support['template'] ) || null === $this->theme_support['template'] ) ? basename( get_template_directory() ) : $this->theme_support['template'];

		$get_category_url = trailingslashit( PIXELGRADE_CARE__API_BASE ) . 'wp-json/pxm/v1/front/get_htkb_categories?kb_current_product_sku=' . $slug;

		$categories = wp_remote_get( $get_category_url, $args );

		if ( is_wp_error( $categories ) ) {
			return array();
		}

		$response = json_decode( wp_remote_retrieve_body( $categories ), true );

		return $response;
	}

	/**
	 * Helper function that sends the System Status Data to our Dashboard
	 */
	function get_install_data() {

		if ( ! isset( $this->options['allow_data_collect'] ) ) {
			$this->options['allow_data_collect'] = "true";
			$this->save_options();
		} elseif ( false === $this->options['allow_data_collect'] ) {
			return "false";
		}

		$data     = PixelgradeCare_DataCollector::instance( $this->parent );
		$response = $data->get_post_data();

		return json_encode( $response );
	}

	function transient_update_theme_version( $transient ) {

		// Nothing to do here if the checked transient entry is empty
		if ( empty( $transient->checked ) ) {
			return $transient;
		}

		// Let's start gathering data about the theme
		// First get the theme directory name (the theme slug - unique)
		$slug = basename( get_template_directory() );

		$theme_data['new_version'] = '';

		// If we have received an update response with a version, save it
		if ( ! empty( $transient->response[ $slug ] ) ) {
			$theme_data['new_version'] = $transient->response[ $slug ]['new_version'];
		}

		set_theme_mod( 'pixcare_new_theme_version', $theme_data['new_version'] );

		return $transient;
	}

	function transient_remove_theme_version( $transient ) {
		remove_theme_mod( 'pixcare_new_theme_version' );
	}

	/**
	 * Returns the config resulted from merging the default config with the remote one
	 * @return array|bool|mixed|object|string
	 */
	private function get_config() {
		$config = get_theme_mod( 'pixcare_theme_config' );

		// Get a default config
		$default_config = $this->get_default_config();

		if ( empty( $config ) ) {
			$config = $this->get_remote_config();
		}

		// if the config contains the Setup Wizard -> STart step remove it
		if ( isset( $config['setupWizard'] ) && isset( $config['setupWizard']['start'] ) ) {
			unset( $config['setupWizard']['start'] );
		}

		// If the remote config does not contain a starter contain replace the starter content step
		// @TODO the remote config is kind of broken atm. That should be fixed. DOing this until the steps are in the correct order on the remote config.
		$theme_id = $this->get_theme_hash_id();
		if ( ! isset( $config['starterContent'] ) && ! empty( $theme_id ) ) {
			unset( $default_config['setupWizard']['support'] );
			if ( $config['setupWizard']['ready'] ) {
				unset( $default_config['setupWizard']['ready'] );
			} else {
				$config['setupWizard']['ready'] = $default_config['setupWizard']['ready'];
				unset( $default_config['setupWizard']['ready'] );
			}
		}

		// If the active theme is a pixelgrade theme - remove the theme step
		if ( $this->is_pixelgrade_theme() ) {
			unset( $default_config['setupWizard']['theme'] );
		}

		if ( empty( $config ) || ! is_array( $config ) ) {
			return $default_config;
		}

		return $this->array_merge_recursive_ex( $default_config, $config );
	}

	/**
	 * Merge two arrays recursively first by key
	 *
	 * An entry can be specifically removed if in the first array `null` is given as value
	 *
	 * @param array $array1
	 * @param array $array2
	 *
	 * @return array
	 */
	function array_merge_recursive_ex( array & $array1, array & $array2 ) {
		$merged = $array1;

		foreach ( $array2 as $key => & $value ) {
			if ( is_array( $value ) && isset( $merged[ $key ] ) && is_array( $merged[ $key ] ) ) {
				$merged[ $key ] = $this->array_merge_recursive_ex( $merged[ $key ], $value );
			} else if ( is_numeric( $key ) ) {
				if ( ! in_array( $value, $merged ) ) {
					$merged[] = $value;
				}
			} else if ( null === $value || "null" === $value ) {
				unset( $merged[ $key ] );
			} else {
				$merged[ $key ] = $value;
			}
		}

		return $merged;
	}

	function transient_update_remote_config( $transient ) {
		// Nothing to do here if the checked transient entry is empty
		if ( empty( $transient->checked ) ) {
			return $transient;
		}

		$this->get_remote_config();

		return $transient;
	}

	/**
	 *
	 */
	function transient_update_license_data( $transient ) {

		// Nothing to do here if the checked transient entry is empty
		if ( empty( $transient->checked ) ) {
			return $transient;
		}

		// Check the status of the user's license
		$this->check_theme_license();

		return $transient;
	}

	/**
	 * Checks the status of the current theme's license
	 */
	function check_theme_license() {
		$theme_id = $this->get_theme_hash_id();

		// get the pixelgrade user id
		$current_user = self::get_theme_activation_user();
		$user_id      = get_user_meta( $current_user->ID, 'pixcare_user_ID', true );

		if ( ! $user_id ) {
			// not authenticated
			return false;
		}

		$get_licenses_url = trailingslashit( PIXELGRADE_CARE__API_BASE ) . 'wp-json/wupl/v1/front/get_licenses';

		$data = array(
			'user_id' => $user_id,
			'hash_id' => $theme_id
		);
		// Get the user's licenses from pixelgrade.com
		$get_licenses = wp_remote_post( $get_licenses_url,
			array(
				'timeout'   => 10,
				'blocking'  => false,
				'body'      => $data,
				'sslverify' => false,
			) );

		if ( is_wp_error( $get_licenses ) ) {
			return false;
		}

		$subscriptions = json_decode( wp_remote_retrieve_body( $get_licenses ), true );

		if ( ! empty( $subscriptions ) ) {
			foreach ( $subscriptions as $key => $value ) {

				if ( ! isset( $value['licenses'] ) || empty( $value['licenses'] ) ) {
					//no licenses found
					return false;
				}

				foreach ( $value['licenses'] as $license ) {
					if ( $license['wupdates_product_hashid'] == $theme_id && $license['license_status'] == 'valid' ) {
						// valid license - we can allow the user to continue
						$this->set_license_mods( $license );

						return true;
					} elseif ( $license['wupdates_product_hashid'] == $theme_id && $license['license_status'] == 'active' ) {
						//the license is in use somewhere else
						$this->set_license_mods( $license );

						return true;
					} elseif ( $license['wupdates_product_hashid'] == $theme_id && ( $license['license_status'] == 'expired' || $license['license_status'] == 'overused' ) ) {
						//the license is expired
						$this->set_license_mods( $license );

						return true;
					} elseif ( $license['license_type'] == 'shop_bundle' && ( $license['license_status'] == 'active' || $license['license_status'] == 'valid' ) ) {
						$this->set_license_mods( $license );

						return true;
					}
				}
			}
		}

		return false;
	}


	/**
	 * A function that will serve the config file for the current theme
	 */
	function get_remote_config() {
		// get the theme
		$theme_support = get_theme_support( 'pixelgrade_care' );

		if ( empty( $theme_support ) ) {
			return false;
		}

		if ( ! $this->options ) {
			$this->options = array();
		}

		$slug = basename( get_template_directory() );

		$ids = apply_filters( 'wupdates_gather_ids', array() );

		if ( ! isset( $ids[ $slug ] ) ) {
			return false;
		}

		$theme_hash_id = $ids[ $slug ]["id"];

		$url = trailingslashit( apply_filters( 'pixelgrade_care_shop_url', PIXELGRADE_CARE__API_BASE ) ) . 'wp-json/pxm/v1/front/get_config?' . 'hash_id=' . $theme_hash_id . '&version=' . $this->pixelgrade_care_manager_api_version;

		$response = wp_remote_get( $url, array(
			'ssl_verify' => false
		) );

		// to check for error
		if ( is_wp_error( $response ) || $response["response"]["code"] !== 200 || ! isset( $response['body'] ) || empty( $response['body'] ) ) {
			// Nada
			return false;
		}

		$config = json_decode( $response['body'], true );

		// such a shim ... keep it just two versions
		if ( ! empty( $config['pixelgrade_care'] ) ) {
			$config['dashboard'] = $config['pixelgrade_care'];
			unset( $config['pixelgrade_care'] );
		}

		set_theme_mod( 'pixcare_theme_config', $config );

		return $config;
	}

	/**
	 * @return mixed
	 * Gets the default, hardcoded config
	 */
	function get_default_config() {
		require_once plugin_dir_path( $this->parent->file ) . 'includes/functions.config.php';
		$original_theme_slug = $this->get_original_theme_slug();

		$config = get_pixelgrade_care_default_config( $original_theme_slug );

		return $config;
	}

	function transient_delete_oauth_token( $transient ) {
		$current_user    = self::get_theme_activation_user();
		$user_token_meta = get_user_meta( $current_user->ID, 'pixcare_oauth_token' );
		$user_pixcare_id = get_user_meta( $current_user->ID, 'pixcare_user_ID' );

		if ( $user_token_meta && empty( $user_pixcare_id ) ) {
			delete_user_meta( $current_user->ID, 'pixcare_oauth_token' );
			delete_user_meta( $current_user->ID, 'pixcare_oauth_token_secret' );
			delete_user_meta( $current_user->ID, 'pixcare_oauth_verifier' );
		}

		return $transient;
	}

	/**
	 * The theme `hash_id` property holds a big responsibility in getting the theme license, so we need to dig for it.
	 * - Priority will have the `theme_support` array if it is there then it is declarative, and it stands.
	 * - The second try will be by getting the style.css main comment and get the template name from there. This is not
	 * reliable since the user can change it.
	 * - The last try will be the theme directory name; also not secure because the user can change it.
	 */
	private function get_theme_hash_id() {
		// Get the id of the current theme
		$wupdates_ids  = apply_filters( 'wupdates_gather_ids', array() );
		$theme_support = get_theme_support( 'pixelgrade_care' );

		// Try to get the theme's name from the theme_supports array.
		if ( isset( $theme_support['theme_name'] ) && ! empty( $wupdates_ids[ $theme_support['theme_name'] ] ) ) {
			return $wupdates_ids[ $theme_support['theme_name'] ];
		}

		// try to get the theme name via the style.css comment
		$theme        = wp_get_theme();
		$maybe_parent = $theme->parent();

		if ( is_child_theme() && ! empty( $maybe_parent ) ) {
			$theme = $maybe_parent;
		}

		$theme_name = strtolower( $theme->get( 'Name' ) );

		if ( ! empty( $wupdates_ids[ $theme_name ] ) ) {
			return $wupdates_ids[ $theme_name ]['id'];
		}

		// try to get it by the theme folder name
		$theme_name = strtolower( basename( get_template_directory() ) );

		if ( ! empty( $wupdates_ids[ $theme_name ] ) ) {
			return $wupdates_ids[ $theme_name ]['id'];
		}

		// no luck, inform the user
		return false;
	}

	/**
	 * Get the theme Original Slug.
	 * @TODO Add tests
	 */
	private function get_original_theme_slug() {
		// Get the id of the current theme
		$wupdates_ids = apply_filters( 'wupdates_gather_ids', array() );
		$slug         = basename( get_template_directory() );

		if ( ! isset( $wupdates_ids[ $slug ] ) || ! isset( $wupdates_ids[ $slug ]['slug'] ) ) {
			return $slug;
		}

		return sanitize_title( $wupdates_ids[ $slug ]['slug'] );
	}

	/**
	 * Get the theme Original Name.
	 * @TODO Add tests
	 */
	private function get_original_theme_name() {
		// Get the id of the current theme
		$wupdates_ids = apply_filters( 'wupdates_gather_ids', array() );
		$slug         = basename( get_template_directory() );

		if ( ! isset( $wupdates_ids[ $slug ] ) || ! isset( $wupdates_ids[ $slug ]['name'] ) ) {
			return ucfirst( $slug );
		}

		return $wupdates_ids[ $slug ]['name'];
	}

	/**
	 * @return bool
	 * Checks if the wupdates_gather_ids filter has been tempered with
	 * This should also be used to block the updates
	 * @TODO Add Tests
	 */
	function is_wupdates_filter_unchanged() {
		// Get the id of the current theme
		$wupdates_ids = apply_filters( 'wupdates_gather_ids', array() );
		$slug         = basename( get_template_directory() );

		// If the user hasn't got any pixelgrade themes - return true. They don't need this filter
		if ( ! $this->has_pixelgrade_theme() ) {
			return true;
		}

		//@TODO FALLBACK - delete after we add the digest to all themes
		// Currently - the older version of some themes do not have the digest param - so we need to overlook it if we want this filter to work.
		if ( ! isset( $wupdates_ids[ $slug ] ) || ! isset( $wupdates_ids[ $slug ]['digest'] ) ) {
			return true;
		}

		// Check if the wupdates_ids array is missing either of this properties
		if ( ! isset( $wupdates_ids[ $slug ] ) || ! isset( $wupdates_ids[ $slug ]['name'] ) || ! isset( $wupdates_ids[ $slug ]['slug'] ) || ! isset( $wupdates_ids[ $slug ]['id'] ) || ! isset( $wupdates_ids[ $slug ]['type'] ) || ! isset( $wupdates_ids[ $slug ]['digest'] ) ) {
			return false;
		}

		// Create the md5 hash from the properties of wupdates_ids and compare it to the digest from that array
		$md5 = md5( 'name-' . $wupdates_ids[ $slug ]['name'] . ';slug-' . $wupdates_ids[ $slug ]['slug'] . ';id-' . $wupdates_ids[ $slug ]['id'] . ';type-' . $wupdates_ids[ $slug ]['type'] );

		// the md5 hash should be the same one as the digest hash
		if ( $md5 !== $wupdates_ids[ $slug ]['digest'] ) {
			return false;
		}

		return true;
	}

	function is_pixelgrade_theme() {
		// Get the id of the current theme
		$wupdates_ids = apply_filters( 'wupdates_gather_ids', array() );
		$slug         = basename( get_template_directory() );

		if ( isset( $wupdates_ids[ $slug ] ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Checks if the theme name's or directory have been changed
	 * Returns an array with two bool values: has_original_name (true | false) and has_original_directory (true | false)
	 * @TODO Add Tests
	 *
	 * @return array|bool
	 */
	function get_theme_checks() {
		$has_original_name      = true;
		$has_original_directory = true;

		// If the user hasn't got any pixelgrade themes - return true. They don't need this filter
		if ( ! $this->has_pixelgrade_theme() ) {
			return array(
				'has_original_name'      => true,
				'has_original_directory' => true
			);
		}

		// Get the id of the current theme
		$wupdates_ids = apply_filters( 'wupdates_gather_ids', array() );
		$slug         = basename( get_template_directory() );

		// Check if the wupdates_ids array is missing either of this properties
		if ( ! isset( $wupdates_ids[ $slug ] ) || ! isset( $wupdates_ids[ $slug ]['name'] ) || ! isset( $wupdates_ids[ $slug ]['slug'] ) || ! isset( $wupdates_ids[ $slug ]['id'] ) || ! isset( $wupdates_ids[ $slug ]['type'] ) || ! isset( $wupdates_ids[ $slug ]['digest'] ) ) {
			return false;
		}

		$hash_id = $wupdates_ids[ $slug ]['id'];
		$type    = $wupdates_ids[ $slug ]['type'];

		// Theme name as is in style.css
		$current_theme         = wp_get_theme( get_template() );
		$theme_stylesheet_name = $current_theme->get( 'Name' );
		$md5                   = md5( 'name-' . $theme_stylesheet_name . ';slug-' . $slug . ';id-' . $hash_id . ';type-' . $type );

		// compare this theme's digest with the one from wupdates. If they're the same all is good.
		// If not - either the theme's Name in style.css or the theme_directory name have been changed
		if ( $md5 === $wupdates_ids[ $slug ]['digest'] ) {
			return array(
				'has_original_name'      => $has_original_name,
				'has_original_directory' => $has_original_directory
			);
		}

		// Check to see if the Theme Name has been changed
		if ( $wupdates_ids[ $slug ]['name'] !== $current_theme->get( 'Name' ) ) {
			$has_original_name = false;
		}

		// Check to see if the Theme Directory has been changed
		if ( $wupdates_ids[ $slug ]['slug'] !== $slug ) {
			$has_original_directory = false;
		}

		return array(
			'has_original_name'      => $has_original_name,
			'has_original_directory' => $has_original_directory
		);
	}

	// Hook to pre_set_site_transient_update_themes and block theme update if directory has been tampered with
	function check_if_update_is_valid( $transient ) {
		$slug = basename( get_template_directory() );

		// If the wupdates_gather_ids filter has been changed - do NOT give access to the update
		if ( ! $this->is_wupdates_filter_unchanged() && property_exists( $transient, 'response' ) && isset( $transient->response[ $slug ] ) ) {
			unset( $transient->response[ $slug ] );
		}

		return $transient;
	}

	function activate_theme_license() {
		$current_user = self::get_theme_activation_user();

		// First off - delete the Pixelgrade themes this user has access to
		delete_user_meta( $current_user->ID, 'pixelgrade_themes' );

		// If they modified anything in the wupdates_gather_ids function - exit.  Cannot activate the theme.
		if ( ! $this->is_wupdates_filter_unchanged() ) {
			return;
		}

		$wupdates_ids = apply_filters( 'wupdates_gather_ids', array() );
		$slug         = basename( get_template_directory() );

		// If the theme is from Pixelgrade - proceed to checking if the pixcare_license_hash theme mod is set
		$pixcare_license_hash = get_theme_mod( 'pixcare_license_hash' );

		// DEtermine wether the user is logged in or not. If not logged in - don't bother trying to activate the theme license
		$pixelgrade_user_id = get_user_meta( $current_user->ID, 'pixcare_user_ID', 1 );

		if ( empty( $pixelgrade_user_id ) ) {
			return;
		}

		if ( isset( $wupdates_ids[ $slug ] ) && empty( $pixcare_license_hash ) ) {
			// Try to activate the theme
			$get_licenses_url = trailingslashit( PIXELGRADE_CARE__API_BASE ) . 'wp-json/wupl/v1/front/get_licenses';
			$data             = array(
				'user_id' => $pixelgrade_user_id,
			);
			if ( isset( $wupdates_ids[ $slug ]['id'] ) ) {
				$data['hash_id'] = $wupdates_ids[ $slug ]['id'];
			}
			$get_licenses_req = wp_remote_post( $get_licenses_url, array(
				'timeout'   => 10,
				'body'      => $data,
				'sslverify' => false,
			) );
			if ( is_wp_error( $get_licenses_req ) ) {
				return;
			}

			$get_licenses_body = json_decode( wp_remote_retrieve_body( $get_licenses_req ), true );

			if ( empty( $get_licenses_body ) || is_wp_error( $get_licenses_body ) ) {
				return;
			}

			$license_key = key( $get_licenses_body );

			if ( isset( $get_licenses_body[ $license_key ]['licenses'] ) && ! empty( $get_licenses_body[ $license_key ]['licenses'] ) ) {
				$licenses         = $get_licenses_body[ $license_key ]['licenses'];
				$valid_licenses   = array();
				$active_licenses  = array();
				$expired_licenses = array();

				// Loop through the licenses and try to find an active or valid one. If none is found - look for expired or overused
				foreach ( $licenses as $license ) {
					switch ( $license['license_status'] ) {
						case 'valid':
							$valid_licenses[] = $license;
							break;
						case 'active':
							$active_licenses[] = $license;
							break;
						case 'expired':
						case 'overused':
							$expired_licenses[] = $license;
							break;
						default:
							break;
					}
				}

				// try to activate a license and save to theme mod
				$license_to_activate = array();
				if ( ! empty( $valid_licenses ) ) {
					$license_to_activate = $valid_licenses[0];
				} elseif ( ! empty( $active_licenses ) ) {
					$license_to_activate = $active_licenses[0];
				} elseif ( ! empty( $expired_licenses ) ) {
					$license_to_activate = $expired_licenses[0];
				}

				// if we have at least one license - go ahead and activate it
				if ( ! empty( $license_to_activate ) ) {
					$activate_license_url = trailingslashit( PIXELGRADE_CARE__API_BASE ) . 'wp-json/wupl/v1/front/license_action';

					// Get all kind of details about the active theme
					$theme_details = $this->get_theme_support();

					$data = array(
						'action'       => 'activate',
						'license_hash' => $license_to_activate['license_hash'],
						'site_url'     => home_url( '/' ),
						'is_ssl'       => is_ssl(),
					);

					if ( isset( $wupdates_ids[ $slug ]['id'] ) ) {
						$data['hash_id'] = $wupdates_ids[ $slug ]['id'];
					}

					if ( isset( $theme_details['theme_version'] ) ) {
						$data['current_version'] = $theme_details['theme_version'];
					}

					$activate_license_req = wp_remote_post( $activate_license_url, array(
						'timeout'   => 10,
						'body'      => $data,
						'sslverify' => false,
					) );

					if ( is_wp_error( $activate_license_req ) ) {
						// do something
					} else {
						$activate_license_body = wp_remote_retrieve_body( $activate_license_req );
						if ( true == $activate_license_body ) {
							// save the theme mods for this license
							$this->set_license_mods( $license_to_activate );
							$this->get_license_themes( $license_to_activate['license_hash'], $pixelgrade_user_id );
						}
					}
				}
			}
		} elseif ( ! empty( $pixcare_license_hash ) ) {
			$this->get_license_themes( $pixcare_license_hash, $pixelgrade_user_id );
		}
	}

	/**
	 * @param $licenseHash
	 *
	 * @return null
	 * A helper function that 'refreshes' the pixelgrade themes available for a given license
	 */
	private function get_license_themes( $licenseHash, $user_id = null ) {
		// Try to get the themes this user has access to
		$get_pixelgrade_themes_url = trailingslashit( PIXELGRADE_CARE__API_BASE ) . 'wp-json/wupl/v1/front/get_customer_products';
		$get_pixelgrade_themes     = wp_remote_post( $get_pixelgrade_themes_url, array(
			'timeout'   => 10,
			'body'      => array(
				'license_hash' => $licenseHash,
				'user_id'      => $user_id
			),
			'sslverify' => false,
		) );

		if ( is_wp_error( $get_pixelgrade_themes ) ) {
			return null;
		}

		$pixelgrade_themes = json_decode( wp_remote_retrieve_body( $get_pixelgrade_themes ), true );
		$this->set_user_themes( $pixelgrade_themes );
	}

	/**
	 * A helper function that sets the license theme mods, to avoid duplicate code.
	 */
	private function set_license_mods( $license ) {
		set_theme_mod( 'pixcare_license_hash', $license['license_hash'] );
		set_theme_mod( 'pixcare_license_status', $license['license_status'] );
		set_theme_mod( 'pixcare_license_type', $license['license_type'] );
		set_theme_mod( 'pixcare_license_expiry_date', $license['license_expiry_date'] );
	}

	function admin_notices() {
		global $pagenow;

		// We only show the update notice on the dashboard
		if ( $pagenow == 'index.php' && current_user_can( 'update_themes' ) ) {
			$new_theme_version = get_theme_mod( 'pixcare_new_theme_version' );
			$theme_name        = $this->get_original_theme_name();
			$theme_support     = $this->get_theme_support();
			if ( ! empty( $new_theme_version ) && ! empty( $theme_name ) && ! empty( $theme_support['theme_version'] ) && true === version_compare( $theme_support['theme_version'], $new_theme_version, '<' ) ) {
				?>
				<div class="notice notice-warning is-dismissible">
					<h3><?php _e( 'New Theme Update is Available!', 'pixelgrade_care' ); ?></h3>
					<hr>
					<p><?php printf( __( 'Great news! A new theme update is available for your <strong>%s</strong> theme, version <strong>%s</strong>. To update go to your <a href="%s">Theme Dashboard</a>.', 'pixelgrade_care' ), $theme_name, $new_theme_version, admin_url( 'admin.php?page=pixelgrade_care' ) ); ?></p>
				</div>
				<?php
			}
		}
	}

	public static function get_theme_activation_user() {
		// Find a user that has the pixelgrade.com connection metas
		$user_query = new WP_User_Query(
			array(
				'meta_query' => array(
					'relation' => 'AND',
					array(
						array(
							'key'     => 'pixelgrade_user_login',
							'compare' => 'EXISTS',
						),
						array(
							'key'     => 'pixelgrade_user_login',
							'value'   => '',
							'compare' => '!=',
						),
					)
				)
			)
		);

		// Get the results from the query, returning the first user
		$users = $user_query->get_results();

		if ( empty( $users ) ) {
			return _wp_get_current_user();
		}

		return reset( $users );

	}

	function get_user_themes() {
		$activation_user = self::get_theme_activation_user();
		$themes          = get_user_meta( $activation_user->ID, 'pixelgrade_themes' );

		if ( empty( $themes ) ) {
			return false;
		}

		return $themes;
	}

	function set_user_themes( $themes ) {
		$activation_user = self::get_theme_activation_user();
		$set_themes      = update_user_meta( $activation_user->ID, 'pixelgrade_themes', self::filter_pixelgrade_themes( $themes ) );

		return $set_themes;
	}

	/**
	 * @param $club_themes
	 *
	 * @return array
	 * A helper functions that builds a specific array of all the pixelgrade themes the user has access to
	 */
	static function filter_pixelgrade_themes( $club_themes ) {
		if ( empty( $club_themes ) ) {
			return $club_themes;
		}

		$themes = array();

		// Loop through the club themes and create wp theme objects for each of them
		foreach ( $club_themes as $key => $club_theme ) {
			$themes[ $key ]['id']                   = isset( $club_theme['slug'] ) ? $club_theme['slug'] : null;
			$themes[ $key ]['active']               = false;
			$themes[ $key ]['name']                 = isset( $club_theme['title'] ) ? $club_theme['title'] : null;
			$themes[ $key ]['screenshot']           = isset( $club_theme['image_html'] ) ? $club_theme['image_html'] : null;
			$themes[ $key ]['hasUpdate']            = false;
			$themes[ $key ]['hasPackage']           = false;
			$themes[ $key ]['author']               = 'pixelgrade';
			$themes[ $key ]['actions']['customize'] = false;
			$themes[ $key ]['installed']            = false;
			$themes[ $key ]['slug']                 = isset( $club_theme['slug'] ) ? $club_theme['slug'] : null;
			$themes[ $key ]['download_url']         = isset( $club_theme['download_url'] ) ? $club_theme['download_url'] : null;
			$themes[ $key ]['demo_url']             = isset( $club_theme['demo_url'] ) ? $club_theme['demo_url'] : null;
			$themes[ $key ]['image_url']            = isset( $club_theme['image_url'] ) ? $club_theme['image_url'] : null;
			$themes[ $key ]['hash_id']              = isset( $club_theme['hash_id'] ) ? $club_theme['hash_id'] : null;
		}

		return $themes;
	}

	function pixcare_update_tgmpa_required_plugins( $tgmpa ) {
		if ( ! empty( $_POST['pixcare_request'] ) && $_POST['pixcare_request'] == 'required_plugins' ) {
			add_filter( 'tgmpa_load', '__return_true' );
		}
	}

	/**
	 * Main PixelgradeCareAdmin Instance
	 *
	 * Ensures only one instance of PixelgradeCareAdmin is loaded or can be loaded.
	 *
	 * @since  1.3.0
	 * @static
	 *
	 * @param  object $parent Main PixelgradeCare instance.
	 *
	 * @return object Main PixelgradeCareAdmin instance
	 */
	public static function instance( $parent ) {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}

		return self::$_instance;
	} // End instance().

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {

		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cheatin&#8217; huh?' ) ), esc_html( $this->parent->get_version() ) );
	} // End __clone().

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {

		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cheatin&#8217; huh?' ) ), esc_html( $this->parent->get_version() ) );
	} // End __wakeup().
}
