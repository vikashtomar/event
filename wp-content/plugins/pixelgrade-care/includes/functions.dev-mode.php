<?php
/**
 * Some functions that help us with the development mode
 *
 * @global PIXELGRADE_CARE__DEV_MODE
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// If we are not in development mode, abort
if ( ! defined( 'PIXELGRADE_CARE__DEV_MODE' ) || PIXELGRADE_CARE__DEV_MODE == false ) {
	return;
}
