<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    PixelgradeCare
 * @subpackage PixelgradeCare/includes
 * @author     Pixelgrade <email@example.com>
 */
class PixelgradeCare {
	/**
	 * The main plugin file.
	 * @var     string
	 * @access  public
	 * @since   1.3.0
	 */
	public $file;

	/**
	 * @var PixelgradeCareAdmin
	 */
	public $plugin_admin = null;

	/**
	 * @var PixelgradeCare_i18n
	 */
	public $plugin_i18n = null;

	/**
	 * @var PixelgradeCareStarterContent
	 */
	public $starter_content = null;

	/**
	 * @var PixelgradeCare_Support
	 */
	public $plugin_support = null;

	/**
	 * @var PixelgradeCareSetupWizard
	 */
	public $plugin_setup_wizard = null;

	/**
	 * @var PixelgradeCare_Club
	 */
	public $plugin_pixelgrade_club = null;

	/**
	 * @var PixelgradeCare_DataCollector
	 */
	public $plugin_data_collector = null;

	/**
	 * The only instance.
	 * @var     PixelgradeCare
	 * @access  protected
	 * @since   1.3.0
	 */
	protected static $_instance = null;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $plugin_name The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * The lowest supported WordPress version
	 * @var string
	 */
	protected $wp_support = '4.6';

	protected $theme_support = false;

	/**
	 * Minimal Required PHP Version
	 * @var string
	 * @access  private
	 * @since   1.3.0
	 */
	private $minimalRequiredPhpVersion  = '5.2.4';

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 *
	 * @param string $file The main plugin file
	 * @param string $version The current version of the plugin
	 */
	public function __construct( $file, $version = '1.0.0' ) {
		//the main plugin file (the one that loads all this)
		$this->file = $file;
		//the current plugin version
		$this->version = $version;

		$this->plugin_name = 'pixelgrade_care';

		if ( $this->php_version_check() ) {
			// Only load and run the init function if we know PHP version can parse it.
			$this->init();
		}
	}

	/**
	 * Initialize plugin
	 */
	private function init() {

		//Handle the install and uninstall logic
		register_activation_hook( $this->file, array( 'PixelgradeCare', 'install' ) );
		register_deactivation_hook( $this->file, array( 'PixelgradeCare', 'uninstall' ) );

		if ( $this->is_wp_compatible() ) {
			$this->load_dependencies();
			$this->set_locale();
			$this->define_admin_hooks();
		} else {
			add_action( 'admin_notices', array( $this, 'add_incompatibility_notice' ) );
		}
	}

	function add_incompatibility_notice() {
		global $wp_version;

		printf(
			'<div class="%1$s"><p><strong>%2$s %3$s %4$s </strong></p><p>%5$s %6$s %7$s</p></div>',
			esc_attr( 'notice notice-error' ),
			esc_html__( "Pixelgrade Themes requires WordPress version", 'pixcare' ),
			$this->wp_support,
			esc_html__( "or later", 'pixcare' ),
			esc_html__( 'You\'re using an old version of WordPress', 'pixcare' ),
			$wp_version,
			esc_html__( 'which is not compatible with the current theme. Please update to the latest version to benefit from all its features.', 'pixcare' )
		);
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - PixelgradeCare_i18n. Defines internationalization functionality.
	 * - PixelgradeCareAdmin. Defines all hooks for the admin area.
	 * - PixelgradeCare_Public. Defines all hooks for the public side of the site.
	 *
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( $this->file ) . 'includes/class-pixelgrade_care-i18n.php';
		$this->plugin_i18n = PixelgradeCare_i18n::instance( $this );

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( $this->file ) . 'admin/class-pixelgrade_care-admin.php';
		$this->plugin_admin = PixelgradeCareAdmin::instance( $this );

		/**
		 * Import demo-data system
		 */
		require_once plugin_dir_path( $this->file ) . 'admin/class-pixelgrade_care-starter_content.php';
		$this->starter_content = PixelgradeCareStarterContent::instance( $this );

		/**
		 * The class responsible for defining all actions that occur in the setup wizard.
		 */
		require_once plugin_dir_path( $this->file ) . 'admin/class-pixelgrade_care-setup.php';
		$this->plugin_setup_wizard = PixelgradeCareSetupWizard::instance( $this );

		/**
		 * The class responsible for defining all actions that occur in the data collection section.
		 */
		require_once plugin_dir_path( $this->file ) . 'includes/class-pixelgrade_care-data-collector.php';

		/**
		 * The class responsible for defining all actions that occur in support section.
		 */
		require_once plugin_dir_path( $this->file ) . 'admin/class-pixelgrade_care-support.php';
		$this->plugin_support = PixelgradeCare_Support::instance( $this );

		/**
		 * The class responsible for defining all actions that occur in the pixelgrade club section.
		 */
		require_once plugin_dir_path( $this->file ) . 'admin/class-pixelgrade_care-club.php';
		$this->plugin_pixelgrade_club = PixelgradeCare_Club::instance( $this );

		/**
		 * Various functionality that helps our themes play nice with other themes and the whole ecosystem in general
		 * Think of custom post types, shortcodes, and so on. Things that are not theme territory.
		 */
		require_once plugin_dir_path( $this->file ) . 'theme-helpers.php';
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the PixelgradeCare_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		add_action( 'plugins_loaded', array( $this->plugin_i18n, 'load_plugin_textdomain' ) );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	public function define_admin_hooks() {
		add_action( 'admin_menu', array( $this->plugin_admin, 'add_pixelgrade_menu' ) );
//		add_action( 'admin_menu', array( $this->plugin_admin, 'add_club_menu' ) );

		add_action( 'admin_init', array( $this->plugin_admin, 'settings_init' ) );
		add_action( 'after_setup_theme', array( $this->plugin_admin, 'init_knowledgeBase_categories' ) );

		add_action( 'current_screen', array( $this->plugin_admin, 'add_tabs' ) );

		// We we will remember the theme version when the transient is updated
		add_filter( 'pre_set_site_transient_update_themes', array(
			$this->plugin_admin,
			'transient_update_theme_version'
		), 11 );
		// We will remove the the info when the transient is deleted (maybe after a successful update?)
		add_action( 'delete_site_transient_update_themes', array(
			$this->plugin_admin,
			'transient_remove_theme_version'
		), 10 );
		add_filter( 'pre_set_site_transient_update_themes', array(
			$this->plugin_admin,
			'transient_update_remote_config'
		), 12 );
		add_filter( 'pre_set_site_transient_update_themes', array(
			$this->plugin_admin,
			'transient_update_kb_categories'
		), 13 );
		add_filter( 'pre_set_site_transient_update_themes', array(
			$this->plugin_admin,
			'transient_delete_oauth_token'
		), 14 );
		add_filter( 'pre_set_site_transient_update_themes', array(
			$this->plugin_admin,
			'transient_update_license_data'
		), 15 );

		add_filter( 'pre_set_theme_mod_pixcare_theme_config', array(
			$this->plugin_admin,
			"sanitize_theme_mods_holding_content"
		), 10, 2 );
		add_filter( 'pre_set_theme_mod_pixcare_support', array(
			$this->plugin_admin,
			"sanitize_theme_mods_holding_content"
		), 10, 2 );

		// Hook to update the Pixelgrade themes a user has access to
		add_filter( 'pre_set_site_transient_update_themes', array( 'PixelgradeCare_Club', 'update_user_themes' ), 10, 2 );

		add_action( 'admin_enqueue_scripts', array( $this->plugin_admin, 'enqueue_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this->plugin_admin, 'enqueue_scripts' ) );
		add_action( 'after_switch_theme', array( $this->plugin_admin, 'activate_theme_license' ) );

		add_action( 'admin_footer', array( $this->plugin_support, 'support_setup' ) );
		add_action( 'customize_controls_enqueue_scripts', array( $this->plugin_support, 'support_setup' ) );

		add_action( 'current_screen', array( $this->plugin_setup_wizard, 'add_tabs' ) );
		add_action( 'admin_menu', array( $this->plugin_setup_wizard, 'add_admin_menu' ) );
		add_action( 'admin_init', array( $this->plugin_setup_wizard, 'setup_wizard' ) );


		// Enqueue the Pixelgrade Club scripts
		add_action( 'wp_footer', array( $this->plugin_pixelgrade_club, 'club_enqueue_fe_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this->plugin_pixelgrade_club, 'club_enqueue_admin_scripts' ) );
		add_action( 'customize_controls_enqueue_scripts', array( $this->plugin_pixelgrade_club, 'club_enqueue_admin_scripts' ) );

		$data_collect_value = get_option( 'pixcare_options' );

		if ( isset( $data_collect_value['allow_data_collect'] ) && $data_collect_value['allow_data_collect'] == true ) {
			add_action( 'admin_init', array( $this, 'init_colector' ), 9 );
		}
	}

	function init_colector() {
		// Initialize the data collector
		$this->plugin_data_collector = PixelgradeCare_DataCollector::instance( $this );

		// add filter to update wordpress minimums
		add_filter( 'pre_set_site_transient_update_themes', array(
			$this->plugin_data_collector,
			'set_core_supported_versions'
		) );
		// Filter the data that is being sent to WUpdates so we can store it in DataVault
		add_filter( 'wupdates_call_data_request', array(
			$this->plugin_data_collector,
			'filter_wupdates_request_data'
		), 11, 2 );
	}

	/*
	 * Install everything needed
	 */
	static public function install() {
		/** @var PixelgradeCare $local_plugin */
		$local_plugin = PixelgradeCare();

		require_once plugin_dir_path( $local_plugin->file ) . 'includes/class-pixelgrade_care-activator.php';
		PixelgradeCareActivator::activate();
	}

	/*
	 * Uninstall everything needed
	 */
	static public function uninstall() {
		/** @var PixelgradeCare $local_plugin */
		$local_plugin = PixelgradeCare();

		require_once plugin_dir_path( $local_plugin->file ) . 'includes/class-pixelgrade_care-deactivator.php';
		PixelgradeCareDeactivator::deactivate();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	public function get_theme_config() {
		$this->plugin_admin->get_remote_config();
	}

	function is_wp_compatible() {
		global $wp_version;

		if ( version_compare( $wp_version, $this->wp_support, '>=' ) ) {
			return true;
		}

		return false;
	}

	/**
	 * PHP version check
	 */
	protected function php_version_check() {

		if ( version_compare( phpversion(), $this->minimalRequiredPhpVersion ) < 0 ) {
			add_action( 'admin_notices', array( $this, 'notice_php_version_wrong' ) );
			return false;
		}

		return true;
	}

	/**
	 * Main PixelgradeCare Instance
	 *
	 * Ensures only one instance of PixelgradeCare is loaded or can be loaded.
	 *
	 * @since  1.3.0
	 * @static
	 * @param string $file    File.
	 * @param string $version Version.
	 *
	 * @see    PixelgradeCare()
	 * @return object Main PixelgradeCare instance
	 */
	public static function instance( $file = '', $version = '1.0.0' ) {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $file, $version );
		}
		return self::$_instance;
	} // End instance ()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.3.0
	 */
	public function __clone() {

		_doing_it_wrong( __FUNCTION__,esc_html( __( 'Cheatin&#8217; huh?' ) ), esc_html( $this->version ) );
	} // End __clone ()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.3.0
	 */
	public function __wakeup() {

		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cheatin&#8217; huh?' ) ),  esc_html( $this->version ) );
	} // End __wakeup ()
}
